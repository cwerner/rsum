package org.rosi_project.model_management.util.query;

import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.attributes;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.naturalJoin;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.thetaJoin;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.outerJoin;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.outgoing;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.incoming;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.subtype;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.supertype;

import java.io.File;

import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.grammar.CompoundKeepBuilder;
import org.rosi_project.model_sync.model_join.representation.grammar.CompoundKeepExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.AbstractJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.writer.FileBasedModelJoinWriter;

public class ModelJoinCreator {

	public AbstractJoinBuilder createNaturalJoin(String start) {
		ClassResource classRessource = ClassResource.fromQualifiedName(start);
		return naturalJoin().join(classRessource).with(classRessource).as(classRessource);
	}

	public AbstractJoinBuilder createThetaJoin(String start, String condition) {
		ClassResource classRessource = ClassResource.fromQualifiedName(start);
		return thetaJoin().where(condition).join(classRessource).with(classRessource).as(classRessource);
	}

	public AbstractJoinBuilder createOuterJoin(String start, boolean left) {
		ClassResource classRessource = ClassResource.fromQualifiedName(start);
		if (left) {
			return outerJoin().leftOuter().join(classRessource).with(classRessource).as(classRessource);
		} else {
			return outerJoin().rightOuter().join(classRessource).with(classRessource).as(classRessource);
		}
	}

	public void createAttribute(Object o, String ressourceName, String attrName) {
		ClassResource classRessource = ClassResource.fromQualifiedName(ressourceName);
		AttributePath attrPath = AttributePath.from(classRessource, attrName);
		KeepExpression ke = attributes(attrPath);
		if (o instanceof AbstractJoinBuilder) {
			((AbstractJoinBuilder) o).keep(ke);
		}
		if (o instanceof CompoundKeepBuilder) {
			((CompoundKeepBuilder) o).keep(ke);
		}
	}

	public CompoundKeepBuilder createOutgoingReference(String fromRessource, String toRessource, String attrName) {
		ClassResource toClassRes = ClassResource.fromQualifiedName(toRessource);
		ClassResource fromClassRes = ClassResource.fromQualifiedName(fromRessource);
		AttributePath attrPath = AttributePath.from(fromClassRes, attrName);
		return outgoing(attrPath).as(toClassRes);
	}

	public CompoundKeepBuilder createIncomingReference(String fromRessource, String toRessource, String attrName) {
		ClassResource toClassRes = ClassResource.fromQualifiedName(toRessource);
		ClassResource fromClassRes = ClassResource.fromQualifiedName(fromRessource);
		AttributePath attrPath = AttributePath.from(fromClassRes, attrName);
		return incoming(attrPath).as(toClassRes);
	}

	public CompoundKeepBuilder createSubType(String ressourceName) {
		ClassResource classRessource = ClassResource.fromQualifiedName(ressourceName);
		return subtype(classRessource).as(classRessource);
	}

	public CompoundKeepBuilder createSuperType(String ressourceName) {
		ClassResource classRessource = ClassResource.fromQualifiedName(ressourceName);
		return supertype(classRessource).as(classRessource);
	}
	
	public void addKeepExpression(Object o, KeepExpression ke) {
		if (o instanceof AbstractJoinBuilder) {
			((AbstractJoinBuilder) o).keep(ke);
		}
		if (o instanceof CompoundKeepBuilder) {
			((CompoundKeepBuilder) o).keep(ke);
		}
	}
	
	public KeepExpression buildExpression(Object o) {
		if (o instanceof CompoundKeepBuilder) {
			return ((CompoundKeepBuilder) o).buildExpression();
		}
		return null;
	}
	
	public JoinExpression done(Object o) {
		if (o instanceof AbstractJoinBuilder) {
			return ((AbstractJoinBuilder) o).done();
		}
		return null;
	}
	
	public Object combine(Object current, Object added) {
		JoinExpression realAdded = (JoinExpression)added;
		if (current instanceof JoinExpression) {
			JoinExpression realCurrent = (JoinExpression)current;
			System.out.println("ClassName: " + realCurrent.getBaseModel().toString());
			AbstractJoinBuilder ajb = createNaturalJoin(realCurrent.getBaseModel().toString());
			for (KeepExpression ke : realCurrent.getKeepsList()) {
				ajb.keep(ke);
			}
			for (KeepExpression ke : realAdded.getKeepsList()) {
				ajb.keep(ke);
			}
			return ajb.done();
		}
		if (current instanceof CompoundKeepExpression) { //createOutgoingReference(String ressourceName, String attrName)
			CompoundKeepExpression realCurrent = (CompoundKeepExpression)current;
			for (KeepExpression ke : realAdded.getKeepsList()) {
				if (!realCurrent.getKeeps().contains(ke)) {
					realCurrent.addKeepExpression(ke);
				}
			}
			return realCurrent;
		}
		return null;
	}

	public void createFile(Object joinExpression, String fileName) {
		if (joinExpression instanceof JoinExpression) {
			JoinExpression je = (JoinExpression) joinExpression;
			ModelJoinExpression modelJoin = ModelJoinBuilder.createNewModelJoin().add(je).build();

			File file = new File(fileName);

			FileBasedModelJoinWriter writer = new FileBasedModelJoinWriter(file);
			writer.write(modelJoin);
		}
	}
}
