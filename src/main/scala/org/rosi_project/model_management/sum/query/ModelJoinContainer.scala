package org.rosi_project.model_management.sum.query

import org.rosi_project.model_management.util.query.ModelJoinCreator

class ModelJoinContainer(val name: String, val source: String) {

  var connect: Map[String, Object] = Map()

  def isComparable(mjc: ModelJoinContainer): Boolean = {
    var result = false
    val size = mjc.connect.keySet.filter(connect.keySet.contains(_)).size
    if (size == 1) {
      val s = mjc.connect.keySet.filter(connect.keySet.contains(_)).head
      if (s == source || s == mjc.source) {
        result = true
      }
    }
    result
  }

  /**
   * Return the container that is not needed anymore.
   */
  def concat(mjc: ModelJoinContainer): ModelJoinContainer = {
    val creator = new ModelJoinCreator
    val s: String = mjc.connect.keySet.filter(connect.keySet.contains(_)).head
    if (s == mjc.source) {
      val res = creator.combine(connect.get(s).get, mjc.connect.get(s).get)
      connect += (s -> res)
      return mjc
    } else {
      val res = creator.combine(mjc.connect.get(s).get, connect.get(s).get)
      mjc.connect += (s -> res)
      return this
    }
  }
  
  override def toString(): String = {
    "MJC:" + " source=" + source + " name=" + name + " map=" + connect
  }

}