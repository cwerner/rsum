package org.rosi_project.example

import view.LibraryView
import view.Manager0
import view.LibrarySimple0

object ExampleRSUM extends App {
    
  println("%%%%%%%%%%%%%%%%%%%%%% Create a view with all data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

  var completeView: LibraryView = LibraryView.getNewView()
    
  println("%%%%%%%%%%%%%%%%%%%%%% Create elements in the view %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

  var libRole = completeView.createLibrary("City Library")
  var empRole = completeView.createEmployee(45000, "Bill Smith")
  var manRole = completeView.createEmployee(60000, "Bob Jones")
  libRole.addEmployees(empRole)
  empRole.setManager(manRole)
  
  println(libRole.getNameView())
  
  completeView.printViewRoles()
    
  var list = completeView.getAllViewElements
    
  println("Test1 " + completeView.getElementsWithClassName("LibraryRole"))
  println("Test2 " + completeView.getElementsWithExample(empRole))
  println("Test3 " + completeView.getMapOfElements())

  println("%%%%%%%%%%%%%%%%%%%%%% Activate other views %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

  var managerView: Manager0 = Manager0.getNewView()
  var simpleView: LibrarySimple0 = LibrarySimple0.getNewView()

  completeView.printViewRoles()
  managerView.printViewRoles()
  simpleView.printViewRoles()

  println("%%%%%%%%%%%%%%%%%%%%%% Create a new relation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

  libRole.addEmployees(manRole)

  println("%%%%%%%%%%%%%%%%%%%%%% Change the name of an employee in a view %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

  empRole.setNameView("Max Smith")

  completeView.printViewRoles()
  managerView.printViewRoles()
  simpleView.printViewRoles()

  println("%%%%%%%%%%%%%%%%%%%%%% Remove an employee %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

  //libRole.deleteElement()
  empRole.deleteElement()
  //manRole.deleteElement()
  //hasMan.deleteElement()
  //hasEmp2.deleteElement()

  completeView.printViewRoles()
  managerView.printViewRoles()
  simpleView.printViewRoles()
}