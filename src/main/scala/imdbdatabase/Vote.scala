package imdbdatabase

class Vote(protected var score: Int) {

  def getScore(): Int = {
    score
  }

  def setScore(s: Int): Unit = {
    score = s
  }

  override def toString(): String = {
    "Vote:" + " score=" + score
  }

}



    