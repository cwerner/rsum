package imdbdatabase

import org.rosi_project.model_management.sum.compartments.IAssociation

class FigurePlayedByActor(private val sInstance: Figure, private val tInstance: Actor) extends IAssociation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[FigurePlayedByActor " + source + ", " + target + "]"
  }

  def getSourceIns(): Figure = {
    return sInstance
  }

  def getTargetIns(): Actor = {
    return tInstance
  }

  class Source extends IAssociationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IAssociationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    