package imdbdatabase

class User(protected var email: String, protected var userName: String, p_Dob: Double, p_Name: String) extends Person(p_Dob, p_Name) {

  def getEmail(): String = {
    email
  }

  def setEmail(e: String): Unit = {
    email = e
  }

  def getUserName(): String = {
    userName
  }

  def setUserName(u: String): Unit = {
    userName = u
  }

  override def toString(): String = {
    "User:" + " email=" + email + " userName=" + userName + " dob=" + dob + " name=" + name
  }

}



    