package imdbdatabase

import org.rosi_project.model_management.sum.compartments.IComposition

class FilmFiguresFigure(private val sInstance: Film, private val tInstance: Figure) extends IComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[FilmFiguresFigure " + source + ", " + target + "]"
  }

  def getSourceIns(): Film = {
    return sInstance
  }

  def getTargetIns(): Figure = {
    return tInstance
  }

  class Source extends ICompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends ICompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    