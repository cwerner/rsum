package imdbdatabase

class Film(protected var year: Int, protected var title: String) {

  def getYear(): Int = {
    year
  }

  def setYear(y: Int): Unit = {
    year = y
  }

  def getTitle(): String = {
    title
  }

  def setTitle(t: String): Unit = {
    title = t
  }

  override def toString(): String = {
    "Film:" + " year=" + year + " title=" + title
  }

}



    