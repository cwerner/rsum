package imdbdatabase

import org.rosi_project.model_management.sum.compartments.IDirectAssoziation

class VoteUserUser(private val sInstance: Vote, private val tInstance: User) extends IDirectAssoziation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[VoteUserUser " + source + ", " + target + "]"
  }

  def getSourceIns(): Vote = {
    return sInstance
  }

  def getTargetIns(): User = {
    return tInstance
  }

  class Source extends IDirectAssoziationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectAssoziationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    