package imdbdatabase

import org.rosi_project.model_management.sum.compartments.IAssociation

class VoteFilmFilm(private val sInstance: Vote, private val tInstance: Film) extends IAssociation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[VoteFilmFilm " + source + ", " + target + "]"
  }

  def getSourceIns(): Vote = {
    return sInstance
  }

  def getTargetIns(): Film = {
    return tInstance
  }

  class Source extends IAssociationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IAssociationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    