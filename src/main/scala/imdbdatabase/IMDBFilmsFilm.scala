package imdbdatabase

import org.rosi_project.model_management.sum.compartments.IComposition

class IMDBFilmsFilm(private val sInstance: IMDB, private val tInstance: Film) extends IComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[IMDBFilmsFilm " + source + ", " + target + "]"
  }

  def getSourceIns(): IMDB = {
    return sInstance
  }

  def getTargetIns(): Film = {
    return tInstance
  }

  class Source extends ICompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends ICompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    