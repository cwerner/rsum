package imdbdatabase

class Person(protected var dob: Double, protected var name: String) {

  def getDob(): Double = {
    dob
  }

  def setDob(d: Double): Unit = {
    dob = d
  }

  def getName(): String = {
    name
  }

  def setName(n: String): Unit = {
    name = n
  }

  override def toString(): String = {
    "Person:" + " dob=" + dob + " name=" + name
  }

}



    