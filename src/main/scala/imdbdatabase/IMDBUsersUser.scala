package imdbdatabase

import org.rosi_project.model_management.sum.compartments.IComposition

class IMDBUsersUser(private val sInstance: IMDB, private val tInstance: User) extends IComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[IMDBUsersUser " + source + ", " + target + "]"
  }

  def getSourceIns(): IMDB = {
    return sInstance
  }

  def getTargetIns(): User = {
    return tInstance
  }

  class Source extends ICompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends ICompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    