package joins

import org.rosi_project.model_management.sum.join.IJoinCompartment
import imdbdatabase.Film
import elib.VideoCassette
import org.rosi_project.model_management.sum.join.IJoinInfo
import java.util.Date

class JoinMovie(private val base: Film, private val other: VideoCassette) extends IJoinCompartment {

  initialize(base, other)

  def getJoinInfo(): IJoinInfo = {
    JoinMovieObject
  }

  def getTitle(): String = {
    +baseRole getTitle ()
  }

  def setTitleView(title: String): Unit = {
    +baseRole setTitle (title)
    if (otherObj != null) {
      +otherRole setTitle (title)
    }
  }

  def getYear(): Int = {
    +baseRole getYear ()
  }

  def setYear(year: Int): Unit = {
    +baseRole setYear (year)
  }

  def getDamaged(): Boolean = {
    if (otherObj != null) {
      return +otherRole getDamaged ()
    }
    return false
  }

  def setDamagedView(damaged: Boolean): Unit = {
    if (otherObj != null) {
      +otherRole setDamaged (damaged)
    }
  }

  def getMinutesLength(): Int = {
    if (otherObj != null) {
      return +otherRole getMinutesLength ()
    }
    return 0
  }

  def setMinutesLengthView(minuteslength: Int): Unit = {
    if (otherObj != null) {
      +otherRole setMinutesLength (minuteslength)
    }
  }

  def getPublicationDate(): Date = {
    if (otherObj != null) {
      return +otherRole getPublicationDate ()
    }
    return null
  }

  def setPublicationDateView(publicationdate: Date): Unit = {
    if (otherObj != null) {
      +otherRole setPublicationDate (publicationdate)
    }
  }

  override def toString(): String = {
    "JOIN JoinMovie: " + baseObj + " " + otherObj
  }

}



    