package joins

import org.rosi_project.model_management.sum.join.IJoinCompartment
import org.rosi_project.model_management.sum.join.RsumJoinType
import imdbdatabase.Film
import elib.VideoCassette
import org.rosi_project.model_management.sum.join.IJoinInfo

object JoinMovieObject extends IJoinInfo {

  def getJoinType(): RsumJoinType.Value = {
    RsumJoinType.natural
  }

  def isInstanceBaseModel(obj: Object): Boolean = {
    obj.isInstanceOf[Film]
  }

  def isInstanceOtherModel(obj: Object): Boolean = {
    obj.isInstanceOf[VideoCassette]
  }

  def getNewInstance(b: Object, o: Object): IJoinCompartment = {
    val j = new JoinMovie(null, null)
    objectInitialize(j, b, o)
    j
  }

  def isInstanceOf(obj: Object): Boolean = {
    obj.isInstanceOf[JoinMovie]
  }

  def matchTwoObjects(b: Object, o: Object): Boolean = {
    val base = b.asInstanceOf[Film]
    val other = o.asInstanceOf[VideoCassette]
    base.getTitle() == other.getTitle()
  }

  override def toString(): String = {
    "JoinMovieObject"
  }

}



    