package elib

import org.rosi_project.model_management.sum.compartments.IDirectAssoziation

class VideoCassetteCastPerson(private val sInstance: VideoCassette, private val tInstance: Person) extends IDirectAssoziation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[VideoCassetteCastPerson " + source + ", " + target + "]"
  }

  def getSourceIns(): VideoCassette = {
    return sInstance
  }

  def getTargetIns(): Person = {
    return tInstance
  }

  class Source extends IDirectAssoziationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectAssoziationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    