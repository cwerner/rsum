package elib

class Person(protected var lastName: String, protected var firstName: String) {

  def getLastName(): String = {
    lastName
  }

  def setLastName(l: String): Unit = {
    lastName = l
  }

  def getFirstName(): String = {
    firstName
  }

  def setFirstName(f: String): Unit = {
    firstName = f
  }

  override def toString(): String = {
    "Person:" + " lastName=" + lastName + " firstName=" + firstName
  }

}



    