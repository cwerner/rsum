package elib

import org.rosi_project.model_management.sum.compartments.IDirectAssoziation

class EmployeeManagerEmployee(private val sInstance: Employee, private val tInstance: Employee) extends IDirectAssoziation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[EmployeeManagerEmployee " + source + ", " + target + "]"
  }

  def getSourceIns(): Employee = {
    return sInstance
  }

  def getTargetIns(): Employee = {
    return tInstance
  }

  class Source extends IDirectAssoziationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectAssoziationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    