package elib

class Borrower(p_LastName: String, p_FirstName: String) extends Person(p_LastName, p_FirstName) {

  override def toString(): String = {
    "Borrower:" + " lastName=" + lastName + " firstName=" + firstName
  }

}



    