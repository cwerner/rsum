package elib

object BookCategory extends Enumeration {

  val Mystery, ScienceFiction, Biography = Value
}

    