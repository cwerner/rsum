package elib

class Employee(p_LastName: String, p_FirstName: String) extends Person(p_LastName, p_FirstName) {

  override def toString(): String = {
    "Employee:" + " lastName=" + lastName + " firstName=" + firstName
  }

}



    