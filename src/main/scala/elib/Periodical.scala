package elib

import java.util.Date

class Periodical(protected var issuesPerYear: Int, protected var title: String, i_PublicationDate: Date) extends Item(i_PublicationDate) {

  def getIssuesPerYear(): Int = {
    issuesPerYear
  }

  def setIssuesPerYear(i: Int): Unit = {
    issuesPerYear = i
  }

  def getTitle(): String = {
    title
  }

  def setTitle(t: String): Unit = {
    title = t
  }

  override def toString(): String = {
    "Periodical:" + " issuesPerYear=" + issuesPerYear + " title=" + title + " publicationDate=" + publicationDate
  }

}



    