package elib

import org.rosi_project.model_management.sum.compartments.IDirectAssoziation

class BookOnTapeAuthorWriter(private val sInstance: BookOnTape, private val tInstance: Writer) extends IDirectAssoziation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[BookOnTapeAuthorWriter " + source + ", " + target + "]"
  }

  def getSourceIns(): BookOnTape = {
    return sInstance
  }

  def getTargetIns(): Writer = {
    return tInstance
  }

  class Source extends IDirectAssoziationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectAssoziationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    