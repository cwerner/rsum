package elib

import org.rosi_project.model_management.sum.compartments.IDirectAssoziation

class LibraryBooksBook(private val sInstance: Library, private val tInstance: Book) extends IDirectAssoziation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[LibraryBooksBook " + source + ", " + target + "]"
  }

  def getSourceIns(): Library = {
    return sInstance
  }

  def getTargetIns(): Book = {
    return tInstance
  }

  class Source extends IDirectAssoziationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectAssoziationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    