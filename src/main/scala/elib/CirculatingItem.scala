package elib

import java.util.Date

class CirculatingItem(i_PublicationDate: Date) extends Item(i_PublicationDate) {

  override def toString(): String = {
    "CirculatingItem:" + " publicationDate=" + publicationDate
  }

}



    