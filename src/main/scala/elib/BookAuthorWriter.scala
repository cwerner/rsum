package elib

import org.rosi_project.model_management.sum.compartments.IAssociation

class BookAuthorWriter(private val sInstance: Book, private val tInstance: Writer) extends IAssociation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[BookAuthorWriter " + source + ", " + target + "]"
  }

  def getSourceIns(): Book = {
    return sInstance
  }

  def getTargetIns(): Writer = {
    return tInstance
  }

  class Source extends IAssociationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IAssociationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    