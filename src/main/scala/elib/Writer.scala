package elib

class Writer(protected var name: String, p_LastName: String, p_FirstName: String) extends Person(p_LastName, p_FirstName) {

  def getName(): String = {
    name
  }

  def setName(n: String): Unit = {
    name = n
  }

  override def toString(): String = {
    "Writer:" + " name=" + name + " lastName=" + lastName + " firstName=" + firstName
  }

}



    