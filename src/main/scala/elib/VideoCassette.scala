package elib

import java.util.Date

class VideoCassette(a_Damaged: Boolean, a_MinutesLength: Int, a_Title: String, a_PublicationDate: Date) extends AudioVisualItem(a_Damaged, a_MinutesLength, a_Title, a_PublicationDate) {

  override def toString(): String = {
    "VideoCassette:" + " damaged=" + damaged + " minutesLength=" + minutesLength + " title=" + title + " publicationDate=" + publicationDate
  }

}



    