package elib

import java.util.Date

class Book(protected var category: BookCategory.Value, protected var pages: Int, protected var title: String, c_PublicationDate: Date) extends CirculatingItem(c_PublicationDate) {

  def getCategory(): BookCategory.Value = {
    category
  }

  def setCategory(c: BookCategory.Value): Unit = {
    category = c
  }

  def getPages(): Int = {
    pages
  }

  def setPages(p: Int): Unit = {
    pages = p
  }

  def getTitle(): String = {
    title
  }

  def setTitle(t: String): Unit = {
    title = t
  }

  override def toString(): String = {
    "Book:" + " category=" + category + " pages=" + pages + " title=" + title + " publicationDate=" + publicationDate
  }

}



    