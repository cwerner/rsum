package elib

import java.util.Date

class Item(protected var publicationDate: Date) {

  def getPublicationDate(): Date = {
    publicationDate
  }

  def setPublicationDate(p: Date): Unit = {
    publicationDate = p
  }

  override def toString(): String = {
    "Item:" + " publicationDate=" + publicationDate
  }

}



    