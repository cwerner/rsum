package elib

import java.util.Date

class AudioVisualItem(protected var damaged: Boolean, protected var minutesLength: Int, protected var title: String, c_PublicationDate: Date) extends CirculatingItem(c_PublicationDate) {

  def getDamaged(): Boolean = {
    damaged
  }

  def setDamaged(d: Boolean): Unit = {
    damaged = d
  }

  def getMinutesLength(): Int = {
    minutesLength
  }

  def setMinutesLength(m: Int): Unit = {
    minutesLength = m
  }

  def getTitle(): String = {
    title
  }

  def setTitle(t: String): Unit = {
    title = t
  }

  override def toString(): String = {
    "AudioVisualItem:" + " damaged=" + damaged + " minutesLength=" + minutesLength + " title=" + title + " publicationDate=" + publicationDate
  }

}



    