package elib

import org.rosi_project.model_management.sum.compartments.IDirectComposition

class LibraryWritersWriter(private val sInstance: Library, private val tInstance: Writer) extends IDirectComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[LibraryWritersWriter " + source + ", " + target + "]"
  }

  def getSourceIns(): Library = {
    return sInstance
  }

  def getTargetIns(): Writer = {
    return tInstance
  }

  class Source extends IDirectCompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectCompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    