package lib

abstract class Person(protected var name: String) {

  def getName(): String = {
    name
  }

  def setName(n: String): Unit = {
    name = n
  }

  override def toString(): String = {
    "Person:" + " name=" + name
  }

}



    