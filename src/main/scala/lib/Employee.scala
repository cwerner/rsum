package lib

class Employee(protected var salary: Double, p_Name: String) extends Person(p_Name) {

  def getSalary(): Double = {
    salary
  }

  def setSalary(s: Double): Unit = {
    salary = s
  }

  override def toString(): String = {
    "Employee:" + " salary=" + salary + " name=" + name
  }

}



    