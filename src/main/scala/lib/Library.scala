package lib

class Library(protected var name: String) {

  def getName(): String = {
    name
  }

  def setName(n: String): Unit = {
    name = n
  }

  override def toString(): String = {
    "Library:" + " name=" + name
  }

}



    