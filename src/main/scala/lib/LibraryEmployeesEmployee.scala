package lib

import org.rosi_project.model_management.sum.compartments.IDirectComposition

class LibraryEmployeesEmployee(private val sInstance: Library, private val tInstance: Employee) extends IDirectComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[LibraryEmployeesEmployee " + source + ", " + target + "]"
  }

  def getSourceIns(): Library = {
    return sInstance
  }

  def getTargetIns(): Employee = {
    return tInstance
  }

  class Source extends IDirectCompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectCompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    