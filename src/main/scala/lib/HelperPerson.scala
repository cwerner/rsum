package lib

import org.rosi_project.model_management.sum.query.QueryHelper

class HelperPerson(p_Name: String) extends Person(p_Name) with QueryHelper {

  override def equals(that: Any): Boolean = {
    that.isInstanceOf[Person]
  }

  override def toString(): String = {
    "HelperPerson:" + " name=" + name
  }

}



    