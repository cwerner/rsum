package view

import elib.BookAuthorWriter
import org.rosi_project.model_management.sum.IViewCompartment
import elib.BookCategory
import elib.BookOnTapeReaderPerson
import elib.Employee
import elib.LibraryBranchesLibrary
import elib.VideoCassette
import elib.BookOnTapeAuthorWriter
import elib.LibraryBooksBook
import elib.LibraryBorrowersBorrower
import elib.Borrower
import elib.LibraryParentBranchLibrary
import elib.Periodical
import elib.Writer
import elib.AudioVisualItem
import elib.EmployeeManagerEmployee
import elib.Person
import java.util.Date
import elib.BookOnTape
import elib.VideoCassetteCastPerson
import elib.Library
import elib.Book
import elib.LibraryWritersWriter
import elib.LibraryEmployeesEmployee
import elib.CirculatingItem
import elib.Item
import elib.LibraryStockItem

import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo

class EclipseLibraryView private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[LibraryStockItem] || className.isInstanceOf[EmployeeManagerEmployee] || className.isInstanceOf[Writer] || className.isInstanceOf[LibraryWritersWriter] || className.isInstanceOf[BookAuthorWriter] || className.isInstanceOf[BookOnTapeAuthorWriter] || className.isInstanceOf[Person] || className.isInstanceOf[LibraryEmployeesEmployee] || className.isInstanceOf[Borrower] || className.isInstanceOf[BookOnTape] || className.isInstanceOf[CirculatingItem] || className.isInstanceOf[Library] || className.isInstanceOf[LibraryBranchesLibrary] || className.isInstanceOf[BookOnTapeReaderPerson] || className.isInstanceOf[Periodical] || className.isInstanceOf[LibraryBooksBook] || className.isInstanceOf[VideoCassette] || className.isInstanceOf[AudioVisualItem] || className.isInstanceOf[Book] || className.isInstanceOf[LibraryParentBranchLibrary] || className.isInstanceOf[Employee] || className.isInstanceOf[LibraryBorrowersBorrower] || className.isInstanceOf[VideoCassetteCastPerson] || className.isInstanceOf[Item]) return true
    return false
  }

  def getViewName(): String = {
    EclipseLibraryView.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[Library]) return new LibraryRole(null)
    if (className.isInstanceOf[Periodical]) return new PeriodicalRole(0, null, null)
    if (className.isInstanceOf[Person]) return new PersonRole(null, null)
    if (className.isInstanceOf[Borrower]) return new BorrowerRole(null, null)
    if (className.isInstanceOf[Item]) return new ItemRole(null)
    if (className.isInstanceOf[AudioVisualItem]) return new AudioVisualItemRole(false, 0, null, null)
    if (className.isInstanceOf[CirculatingItem]) return new CirculatingItemRole(null)
    if (className.isInstanceOf[BookOnTape]) return new BookOnTapeRole(false, 0, null, null)
    if (className.isInstanceOf[VideoCassette]) return new VideoCassetteRole(false, 0, null, null)
    if (className.isInstanceOf[Book]) return new BookRole(null, 0, null, null)
    if (className.isInstanceOf[Writer]) return new WriterRole(null, null, null)
    if (className.isInstanceOf[Employee]) return new EmployeeRole(null, null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[EmployeeManagerEmployee]) return new EmployeeManagerEmployeeRole(sourceRole.asInstanceOf[EmployeeRole], targetRole.asInstanceOf[EmployeeRole])
    if (className.isInstanceOf[LibraryStockItem]) return new LibraryStockItemRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[ItemRole])
    if (className.isInstanceOf[LibraryWritersWriter]) return new LibraryWritersWriterRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[WriterRole])
    if (className.isInstanceOf[LibraryBooksBook]) return new LibraryBooksBookRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[BookRole])
    if (className.isInstanceOf[LibraryParentBranchLibrary]) return new LibraryParentBranchLibraryRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[LibraryRole])
    if (className.isInstanceOf[LibraryBranchesLibrary]) return new LibraryBranchesLibraryRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[LibraryRole])
    if (className.isInstanceOf[LibraryEmployeesEmployee]) return new LibraryEmployeesEmployeeRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[EmployeeRole])
    if (className.isInstanceOf[BookAuthorWriter]) return new BookAuthorWriterRole(sourceRole.asInstanceOf[BookRole], targetRole.asInstanceOf[WriterRole])
    if (className.isInstanceOf[VideoCassetteCastPerson]) return new VideoCassetteCastPersonRole(sourceRole.asInstanceOf[VideoCassetteRole], targetRole.asInstanceOf[PersonRole])
    if (className.isInstanceOf[BookOnTapeAuthorWriter]) return new BookOnTapeAuthorWriterRole(sourceRole.asInstanceOf[BookOnTapeRole], targetRole.asInstanceOf[WriterRole])
    if (className.isInstanceOf[BookOnTapeReaderPerson]) return new BookOnTapeReaderPersonRole(sourceRole.asInstanceOf[BookOnTapeRole], targetRole.asInstanceOf[PersonRole])
    if (className.isInstanceOf[LibraryBorrowersBorrower]) return new LibraryBorrowersBorrowerRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[BorrowerRole])
    return null
  }

  override def toString(): String = {
    "VC: EclipseLibraryView"
  }

  def createLibrary(name: String): EclipseLibraryView#LibraryRole = {
    return new LibraryRole(name)
  }

  def createPeriodical(issuesPerYear: Int, title: String, publicationDate: Date): EclipseLibraryView#PeriodicalRole = {
    return new PeriodicalRole(issuesPerYear, title, publicationDate)
  }

  def createPerson(lastName: String, firstName: String): EclipseLibraryView#PersonRole = {
    return new PersonRole(lastName, firstName)
  }

  def createBorrower(lastName: String, firstName: String): EclipseLibraryView#BorrowerRole = {
    return new BorrowerRole(lastName, firstName)
  }

  def createItem(publicationDate: Date): EclipseLibraryView#ItemRole = {
    return new ItemRole(publicationDate)
  }

  def createAudioVisualItem(damaged: Boolean, minutesLength: Int, title: String, publicationDate: Date): EclipseLibraryView#AudioVisualItemRole = {
    return new AudioVisualItemRole(damaged, minutesLength, title, publicationDate)
  }

  def createCirculatingItem(publicationDate: Date): EclipseLibraryView#CirculatingItemRole = {
    return new CirculatingItemRole(publicationDate)
  }

  def createBookOnTape(damaged: Boolean, minutesLength: Int, title: String, publicationDate: Date): EclipseLibraryView#BookOnTapeRole = {
    return new BookOnTapeRole(damaged, minutesLength, title, publicationDate)
  }

  def createVideoCassette(damaged: Boolean, minutesLength: Int, title: String, publicationDate: Date): EclipseLibraryView#VideoCassetteRole = {
    return new VideoCassetteRole(damaged, minutesLength, title, publicationDate)
  }

  def createBook(category: BookCategory.Value, pages: Int, title: String, publicationDate: Date): EclipseLibraryView#BookRole = {
    return new BookRole(category, pages, title, publicationDate)
  }

  def createWriter(name: String, lastName: String, firstName: String): EclipseLibraryView#WriterRole = {
    return new WriterRole(name, lastName, firstName)
  }

  def createEmployee(lastName: String, firstName: String): EclipseLibraryView#EmployeeRole = {
    return new EmployeeRole(lastName, firstName)
  }

  class ItemRole(protected val publicationDate: Date) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Item(publicationDate)
    }

    override def toString(): String = {
      "VNR: ItemRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getPublicationDateView(): Date = {
      +this getPublicationDate ()
    }

    def setPublicationDateView(publicationdate: Date): Unit = {
      +this setPublicationDate (publicationdate)
      +this changeSomething ()
    }

  }

  class EmployeeRole(p_LastName: String, p_FirstName: String) extends PersonRole(p_LastName, p_FirstName) {

    private var manager: EclipseLibraryView#EmployeeManagerEmployeeRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Employee(lastName, firstName)
    }

    override def toString(): String = {
      "VNR: EmployeeRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[EclipseLibraryView] def removeManagerIntern(v: EclipseLibraryView#EmployeeManagerEmployeeRole): Unit = {
      manager = null
    }

    private[EclipseLibraryView] def setManagerIntern(v: EclipseLibraryView#EmployeeManagerEmployeeRole): Unit = {
      manager = v
    }

    def getManager(): EclipseLibraryView#EmployeeRole = {
      return manager.getTarget()
    }

    def setManager(v: EclipseLibraryView#EmployeeRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (manager != null) {
        if (manager.getTarget() == v) return false
        manager.deleteElement()
      }
      new EmployeeManagerEmployeeRole(this, v)
      return true
    }

  }

  class PeriodicalRole(protected val issuesPerYear: Int, protected val title: String, i_PublicationDate: Date) extends ItemRole(i_PublicationDate) {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Periodical(issuesPerYear, title, publicationDate)
    }

    override def toString(): String = {
      "VNR: PeriodicalRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getIssuesPerYearView(): Int = {
      +this getIssuesPerYear ()
    }

    def setIssuesPerYearView(issuesperyear: Int): Unit = {
      +this setIssuesPerYear (issuesperyear)
      +this changeSomething ()
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String): Unit = {
      +this setTitle (title)
      +this changeSomething ()
    }

  }

  class VideoCassetteRole(a_Damaged: Boolean, a_MinutesLength: Int, a_Title: String, a_PublicationDate: Date) extends AudioVisualItemRole(a_Damaged, a_MinutesLength, a_Title, a_PublicationDate) {

    private var cast: Set[EclipseLibraryView#VideoCassetteCastPersonRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new VideoCassette(damaged, minutesLength, title, null)
    }

    override def toString(): String = {
      "VNR: VideoCassetteRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[EclipseLibraryView] def removeCastIntern(v: EclipseLibraryView#VideoCassetteCastPersonRole): Unit = {
      cast -= v
    }

    private[EclipseLibraryView] def setCastIntern(v: EclipseLibraryView#VideoCassetteCastPersonRole): Unit = {
      cast += v
    }

    def getCast(): Set[EclipseLibraryView#PersonRole] = {
      var vs: Set[EclipseLibraryView#PersonRole] = Set.empty
      cast.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasCast(v: EclipseLibraryView#PersonRole): Boolean = {
      return getCast.contains(v)
    }

    def addCast(v: EclipseLibraryView#PersonRole): Boolean = {
      if (hasCast(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new VideoCassetteCastPersonRole(this, v)
      return true
    }

    def removeCast(v: EclipseLibraryView#PersonRole): Boolean = {
      if (!hasCast(v)) return false
      cast.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class AudioVisualItemRole(protected val damaged: Boolean, protected val minutesLength: Int, protected val title: String, c_PublicationDate: Date) extends CirculatingItemRole(c_PublicationDate) {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new AudioVisualItem(damaged, minutesLength, title, null)
    }

    override def toString(): String = {
      "VNR: AudioVisualItemRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getDamagedView(): Boolean = {
      +this getDamaged ()
    }

    def setDamagedView(damaged: Boolean): Unit = {
      +this setDamaged (damaged)
      +this changeSomething ()
    }

    def getMinutesLengthView(): Int = {
      +this getMinutesLength ()
    }

    def setMinutesLengthView(minuteslength: Int): Unit = {
      +this setMinutesLength (minuteslength)
      +this changeSomething ()
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String): Unit = {
      +this setTitle (title)
      +this changeSomething ()
    }

  }

  class WriterRole(protected val name: String, p_LastName: String, p_FirstName: String) extends PersonRole(p_LastName, p_FirstName) {

    private var books: Set[EclipseLibraryView#BookAuthorWriterRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Writer(name, lastName, firstName)
    }

    override def toString(): String = {
      "VNR: WriterRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

    private[EclipseLibraryView] def removeBooksIntern(v: EclipseLibraryView#BookAuthorWriterRole): Unit = {
      books -= v
    }

    private[EclipseLibraryView] def setBooksIntern(v: EclipseLibraryView#BookAuthorWriterRole): Unit = {
      books += v
    }

    def getBooks(): Set[EclipseLibraryView#BookRole] = {
      var vs: Set[EclipseLibraryView#BookRole] = Set.empty
      books.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasBooks(v: EclipseLibraryView#BookRole): Boolean = {
      return getBooks.contains(v)
    }

    def addBooks(v: EclipseLibraryView#BookRole): Boolean = {
      if (hasBooks(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new BookAuthorWriterRole(v, this)
      return true
    }

    def removeBooks(v: EclipseLibraryView#BookRole): Boolean = {
      if (!hasBooks(v)) return false
      books.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class BookRole(protected val category: BookCategory.Value, protected val pages: Int, protected val title: String, c_PublicationDate: Date) extends CirculatingItemRole(c_PublicationDate) {

    private var author: EclipseLibraryView#BookAuthorWriterRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Book(category, pages, title, null)
    }

    override def toString(): String = {
      "VNR: BookRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getCategoryView(): BookCategory.Value = {
      +this getCategory ()
    }

    def setCategoryView(category: BookCategory.Value): Unit = {
      +this setCategory (category)
      +this changeSomething ()
    }

    def getPagesView(): Int = {
      +this getPages ()
    }

    def setPagesView(pages: Int): Unit = {
      +this setPages (pages)
      +this changeSomething ()
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String): Unit = {
      +this setTitle (title)
      +this changeSomething ()
    }

    private[EclipseLibraryView] def removeAuthorIntern(v: EclipseLibraryView#BookAuthorWriterRole): Unit = {
      author = null
    }

    private[EclipseLibraryView] def setAuthorIntern(v: EclipseLibraryView#BookAuthorWriterRole): Unit = {
      author = v
    }

    def getAuthor(): EclipseLibraryView#WriterRole = {
      return author.getTarget()
    }

    def setAuthor(v: EclipseLibraryView#WriterRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (author != null) {
        if (author.getTarget() == v) return false
        author.deleteElement()
      }
      new BookAuthorWriterRole(this, v)
      return true
    }

  }

  class LibraryRole(protected val name: String) extends AViewRole {

    private var employees: Set[EclipseLibraryView#LibraryEmployeesEmployeeRole] = Set.empty
    private var parentBranch: EclipseLibraryView#LibraryParentBranchLibraryRole = null
    private var writers: Set[EclipseLibraryView#LibraryWritersWriterRole] = Set.empty
    private var borrowers: Set[EclipseLibraryView#LibraryBorrowersBorrowerRole] = Set.empty
    private var stock: Set[EclipseLibraryView#LibraryStockItemRole] = Set.empty
    private var branches: Set[EclipseLibraryView#LibraryBranchesLibraryRole] = Set.empty
    private var books: Set[EclipseLibraryView#LibraryBooksBookRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Library(name)
    }

    override def toString(): String = {
      "VNR: LibraryRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

    private[EclipseLibraryView] def removeEmployeesIntern(v: EclipseLibraryView#LibraryEmployeesEmployeeRole): Unit = {
      employees -= v
    }

    private[EclipseLibraryView] def setEmployeesIntern(v: EclipseLibraryView#LibraryEmployeesEmployeeRole): Unit = {
      employees += v
    }

    def getEmployees(): Set[EclipseLibraryView#EmployeeRole] = {
      var vs: Set[EclipseLibraryView#EmployeeRole] = Set.empty
      employees.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasEmployees(v: EclipseLibraryView#EmployeeRole): Boolean = {
      return getEmployees.contains(v)
    }

    def addEmployees(v: EclipseLibraryView#EmployeeRole): Boolean = {
      if (hasEmployees(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryEmployeesEmployeeRole(this, v)
      return true
    }

    def removeEmployees(v: EclipseLibraryView#EmployeeRole): Boolean = {
      if (!hasEmployees(v)) return false
      employees.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryView] def removeParentBranchIntern(v: EclipseLibraryView#LibraryParentBranchLibraryRole): Unit = {
      parentBranch = null
    }

    private[EclipseLibraryView] def setParentBranchIntern(v: EclipseLibraryView#LibraryParentBranchLibraryRole): Unit = {
      parentBranch = v
    }

    def getParentBranch(): EclipseLibraryView#LibraryRole = {
      return parentBranch.getTarget()
    }

    def setParentBranch(v: EclipseLibraryView#LibraryRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (parentBranch != null) {
        if (parentBranch.getTarget() == v) return false
        parentBranch.deleteElement()
      }
      new LibraryParentBranchLibraryRole(this, v)
      return true
    }

    private[EclipseLibraryView] def removeWritersIntern(v: EclipseLibraryView#LibraryWritersWriterRole): Unit = {
      writers -= v
    }

    private[EclipseLibraryView] def setWritersIntern(v: EclipseLibraryView#LibraryWritersWriterRole): Unit = {
      writers += v
    }

    def getWriters(): Set[EclipseLibraryView#WriterRole] = {
      var vs: Set[EclipseLibraryView#WriterRole] = Set.empty
      writers.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasWriters(v: EclipseLibraryView#WriterRole): Boolean = {
      return getWriters.contains(v)
    }

    def addWriters(v: EclipseLibraryView#WriterRole): Boolean = {
      if (hasWriters(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryWritersWriterRole(this, v)
      return true
    }

    def removeWriters(v: EclipseLibraryView#WriterRole): Boolean = {
      if (!hasWriters(v)) return false
      writers.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryView] def removeBorrowersIntern(v: EclipseLibraryView#LibraryBorrowersBorrowerRole): Unit = {
      borrowers -= v
    }

    private[EclipseLibraryView] def setBorrowersIntern(v: EclipseLibraryView#LibraryBorrowersBorrowerRole): Unit = {
      borrowers += v
    }

    def getBorrowers(): Set[EclipseLibraryView#BorrowerRole] = {
      var vs: Set[EclipseLibraryView#BorrowerRole] = Set.empty
      borrowers.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasBorrowers(v: EclipseLibraryView#BorrowerRole): Boolean = {
      return getBorrowers.contains(v)
    }

    def addBorrowers(v: EclipseLibraryView#BorrowerRole): Boolean = {
      if (hasBorrowers(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryBorrowersBorrowerRole(this, v)
      return true
    }

    def removeBorrowers(v: EclipseLibraryView#BorrowerRole): Boolean = {
      if (!hasBorrowers(v)) return false
      borrowers.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryView] def removeStockIntern(v: EclipseLibraryView#LibraryStockItemRole): Unit = {
      stock -= v
    }

    private[EclipseLibraryView] def setStockIntern(v: EclipseLibraryView#LibraryStockItemRole): Unit = {
      stock += v
    }

    def getStock(): Set[EclipseLibraryView#ItemRole] = {
      var vs: Set[EclipseLibraryView#ItemRole] = Set.empty
      stock.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasStock(v: EclipseLibraryView#ItemRole): Boolean = {
      return getStock.contains(v)
    }

    def addStock(v: EclipseLibraryView#ItemRole): Boolean = {
      if (hasStock(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryStockItemRole(this, v)
      return true
    }

    def removeStock(v: EclipseLibraryView#ItemRole): Boolean = {
      if (!hasStock(v)) return false
      stock.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryView] def removeBranchesIntern(v: EclipseLibraryView#LibraryBranchesLibraryRole): Unit = {
      branches -= v
    }

    private[EclipseLibraryView] def setBranchesIntern(v: EclipseLibraryView#LibraryBranchesLibraryRole): Unit = {
      branches += v
    }

    def getBranches(): Set[EclipseLibraryView#LibraryRole] = {
      var vs: Set[EclipseLibraryView#LibraryRole] = Set.empty
      branches.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasBranches(v: EclipseLibraryView#LibraryRole): Boolean = {
      return getBranches.contains(v)
    }

    def addBranches(v: EclipseLibraryView#LibraryRole): Boolean = {
      if (hasBranches(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryBranchesLibraryRole(this, v)
      return true
    }

    def removeBranches(v: EclipseLibraryView#LibraryRole): Boolean = {
      if (!hasBranches(v)) return false
      branches.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryView] def removeBooksIntern(v: EclipseLibraryView#LibraryBooksBookRole): Unit = {
      books -= v
    }

    private[EclipseLibraryView] def setBooksIntern(v: EclipseLibraryView#LibraryBooksBookRole): Unit = {
      books += v
    }

    def getBooks(): Set[EclipseLibraryView#BookRole] = {
      var vs: Set[EclipseLibraryView#BookRole] = Set.empty
      books.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasBooks(v: EclipseLibraryView#BookRole): Boolean = {
      return getBooks.contains(v)
    }

    def addBooks(v: EclipseLibraryView#BookRole): Boolean = {
      if (hasBooks(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryBooksBookRole(this, v)
      return true
    }

    def removeBooks(v: EclipseLibraryView#BookRole): Boolean = {
      if (!hasBooks(v)) return false
      books.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class BookOnTapeRole(a_Damaged: Boolean, a_MinutesLength: Int, a_Title: String, a_PublicationDate: Date) extends AudioVisualItemRole(a_Damaged, a_MinutesLength, a_Title, a_PublicationDate) {

    private var author: EclipseLibraryView#BookOnTapeAuthorWriterRole = null
    private var reader: EclipseLibraryView#BookOnTapeReaderPersonRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new BookOnTape(damaged, minutesLength, title, null)
    }

    override def toString(): String = {
      "VNR: BookOnTapeRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[EclipseLibraryView] def removeAuthorIntern(v: EclipseLibraryView#BookOnTapeAuthorWriterRole): Unit = {
      author = null
    }

    private[EclipseLibraryView] def setAuthorIntern(v: EclipseLibraryView#BookOnTapeAuthorWriterRole): Unit = {
      author = v
    }

    def getAuthor(): EclipseLibraryView#WriterRole = {
      return author.getTarget()
    }

    def setAuthor(v: EclipseLibraryView#WriterRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (author != null) {
        if (author.getTarget() == v) return false
        author.deleteElement()
      }
      new BookOnTapeAuthorWriterRole(this, v)
      return true
    }

    private[EclipseLibraryView] def removeReaderIntern(v: EclipseLibraryView#BookOnTapeReaderPersonRole): Unit = {
      reader = null
    }

    private[EclipseLibraryView] def setReaderIntern(v: EclipseLibraryView#BookOnTapeReaderPersonRole): Unit = {
      reader = v
    }

    def getReader(): EclipseLibraryView#PersonRole = {
      return reader.getTarget()
    }

    def setReader(v: EclipseLibraryView#PersonRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (reader != null) {
        if (reader.getTarget() == v) return false
        reader.deleteElement()
      }
      new BookOnTapeReaderPersonRole(this, v)
      return true
    }

  }

  class CirculatingItemRole(i_PublicationDate: Date) extends ItemRole(i_PublicationDate) {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new CirculatingItem(publicationDate)
    }

    override def toString(): String = {
      "VNR: CirculatingItemRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

  }

  class BorrowerRole(p_LastName: String, p_FirstName: String) extends PersonRole(p_LastName, p_FirstName) {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Borrower(lastName, firstName)
    }

    override def toString(): String = {
      "VNR: BorrowerRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

  }

  class PersonRole(protected val lastName: String, protected val firstName: String) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Person(lastName, firstName)
    }

    override def toString(): String = {
      "VNR: PersonRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getLastNameView(): String = {
      +this getLastName ()
    }

    def setLastNameView(lastname: String): Unit = {
      +this setLastName (lastname)
      +this changeSomething ()
    }

    def getFirstNameView(): String = {
      +this getFirstName ()
    }

    def setFirstNameView(firstname: String): Unit = {
      +this setFirstName (firstname)
      +this changeSomething ()
    }

  }

  class LibraryEmployeesEmployeeRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#EmployeeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setEmployeesIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Employee = getPlayerOfType(target, "Employee").asInstanceOf[Employee]
      val v: LibraryEmployeesEmployee = new LibraryEmployeesEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeEmployeesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryEmployeesEmployeeRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#EmployeeRole = {
      target
    }

  }

  class LibraryParentBranchLibraryRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#LibraryRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setParentBranchIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Library = getPlayerOfType(target, "Library").asInstanceOf[Library]
      val v: LibraryParentBranchLibrary = new LibraryParentBranchLibrary(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeParentBranchIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryParentBranchLibraryRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#LibraryRole = {
      target
    }

  }

  class LibraryWritersWriterRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#WriterRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setWritersIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Writer = getPlayerOfType(target, "Writer").asInstanceOf[Writer]
      val v: LibraryWritersWriter = new LibraryWritersWriter(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeWritersIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryWritersWriterRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#WriterRole = {
      target
    }

  }

  class BookOnTapeAuthorWriterRole(private val source: EclipseLibraryView#BookOnTapeRole, private val target: EclipseLibraryView#WriterRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setAuthorIntern(this)
      val sp: BookOnTape = getPlayerOfType(source, "BookOnTape").asInstanceOf[BookOnTape]
      val tp: Writer = getPlayerOfType(target, "Writer").asInstanceOf[Writer]
      val v: BookOnTapeAuthorWriter = new BookOnTapeAuthorWriter(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeAuthorIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: BookOnTapeAuthorWriterRole"
    }

    def getSource(): EclipseLibraryView#BookOnTapeRole = {
      source
    }

    def getTarget(): EclipseLibraryView#WriterRole = {
      target
    }

  }

  class BookOnTapeReaderPersonRole(private val source: EclipseLibraryView#BookOnTapeRole, private val target: EclipseLibraryView#PersonRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setReaderIntern(this)
      val sp: BookOnTape = getPlayerOfType(source, "BookOnTape").asInstanceOf[BookOnTape]
      val tp: Person = getPlayerOfType(target, "Person").asInstanceOf[Person]
      val v: BookOnTapeReaderPerson = new BookOnTapeReaderPerson(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeReaderIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: BookOnTapeReaderPersonRole"
    }

    def getSource(): EclipseLibraryView#BookOnTapeRole = {
      source
    }

    def getTarget(): EclipseLibraryView#PersonRole = {
      target
    }

  }

  class LibraryBorrowersBorrowerRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#BorrowerRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBorrowersIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Borrower = getPlayerOfType(target, "Borrower").asInstanceOf[Borrower]
      val v: LibraryBorrowersBorrower = new LibraryBorrowersBorrower(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBorrowersIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryBorrowersBorrowerRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#BorrowerRole = {
      target
    }

  }

  class LibraryStockItemRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#ItemRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setStockIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Item = getPlayerOfType(target, "Item").asInstanceOf[Item]
      val v: LibraryStockItem = new LibraryStockItem(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeStockIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryStockItemRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#ItemRole = {
      target
    }

  }

  class BookAuthorWriterRole(private val source: EclipseLibraryView#BookRole, private val target: EclipseLibraryView#WriterRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setBooksIntern(this)
      source.setAuthorIntern(this)
      val sp: Book = getPlayerOfType(source, "Book").asInstanceOf[Book]
      val tp: Writer = getPlayerOfType(target, "Writer").asInstanceOf[Writer]
      val v: BookAuthorWriter = new BookAuthorWriter(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeAuthorIntern(this)
      target.removeBooksIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: BookAuthorWriterRole"
    }

    def getSource(): EclipseLibraryView#BookRole = {
      source
    }

    def getTarget(): EclipseLibraryView#WriterRole = {
      target
    }

  }

  class LibraryBranchesLibraryRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#LibraryRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBranchesIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Library = getPlayerOfType(target, "Library").asInstanceOf[Library]
      val v: LibraryBranchesLibrary = new LibraryBranchesLibrary(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBranchesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryBranchesLibraryRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#LibraryRole = {
      target
    }

  }

  class VideoCassetteCastPersonRole(private val source: EclipseLibraryView#VideoCassetteRole, private val target: EclipseLibraryView#PersonRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setCastIntern(this)
      val sp: VideoCassette = getPlayerOfType(source, "VideoCassette").asInstanceOf[VideoCassette]
      val tp: Person = getPlayerOfType(target, "Person").asInstanceOf[Person]
      val v: VideoCassetteCastPerson = new VideoCassetteCastPerson(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeCastIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: VideoCassetteCastPersonRole"
    }

    def getSource(): EclipseLibraryView#VideoCassetteRole = {
      source
    }

    def getTarget(): EclipseLibraryView#PersonRole = {
      target
    }

  }

  class EmployeeManagerEmployeeRole(private val source: EclipseLibraryView#EmployeeRole, private val target: EclipseLibraryView#EmployeeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setManagerIntern(this)
      val sp: Employee = getPlayerOfType(source, "Employee").asInstanceOf[Employee]
      val tp: Employee = getPlayerOfType(target, "Employee").asInstanceOf[Employee]
      val v: EmployeeManagerEmployee = new EmployeeManagerEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeManagerIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: EmployeeManagerEmployeeRole"
    }

    def getSource(): EclipseLibraryView#EmployeeRole = {
      source
    }

    def getTarget(): EclipseLibraryView#EmployeeRole = {
      target
    }

  }

  class LibraryBooksBookRole(private val source: EclipseLibraryView#LibraryRole, private val target: EclipseLibraryView#BookRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBooksIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Book = getPlayerOfType(target, "Book").asInstanceOf[Book]
      val v: LibraryBooksBook = new LibraryBooksBook(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBooksIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryBooksBookRole"
    }

    def getSource(): EclipseLibraryView#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryView#BookRole = {
      target
    }

  }

}

object EclipseLibraryView extends IViewTypeInfo {

  override def getViewName(): String = "EclipseLibraryView"

  def getJoinInfos(): Set[IJoinInfo] = Set.empty

  protected def getNewInstance(): IViewCompartment = new EclipseLibraryView()

  def getNewView(): EclipseLibraryView = getNewViewTypeInstance().asInstanceOf[EclipseLibraryView]
}
      

    