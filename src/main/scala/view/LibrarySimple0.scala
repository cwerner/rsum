package view

import org.rosi_project.model_management.sum.IViewCompartment
import lib.Library
import lib.Person
import lib.LibraryEmployeesEmployee
import lib.Employee

import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo

class LibrarySimple0 private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[Library] || className.isInstanceOf[Employee] || className.isInstanceOf[LibraryEmployeesEmployee]) return true
    return false
  }

  def getViewName(): String = {
    LibrarySimple0.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[Library]) return new LibraryRole(null)
    if (className.isInstanceOf[Employee]) return new EmployeeRole(0, null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[LibraryEmployeesEmployee]) return new LibraryEmployeesEmployeeRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[EmployeeRole])
    return null
  }

  override def toString(): String = {
    "VC: LibrarySimple0"
  }

  def createLibrary(name: String): LibrarySimple0#LibraryRole = {
    return new LibraryRole(name)
  }

  def createEmployee(salary: Double, name: String): LibrarySimple0#EmployeeRole = {
    return new EmployeeRole(salary, name)
  }

  class LibraryRole(protected val name: String) extends AViewRole {

    private var employees: Set[LibrarySimple0#LibraryEmployeesEmployeeRole] = Set.empty

    private[LibrarySimple0] def removeEmployeesIntern(v: LibrarySimple0#LibraryEmployeesEmployeeRole): Unit = {
      employees -= v
    }

    private[LibrarySimple0] def setEmployeesIntern(v: LibrarySimple0#LibraryEmployeesEmployeeRole): Unit = {
      employees += v
    }

    def getEmployees(): Set[LibrarySimple0#EmployeeRole] = {
      var vs: Set[LibrarySimple0#EmployeeRole] = Set.empty
      employees.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasEmployees(v: LibrarySimple0#EmployeeRole): Boolean = {
      return getEmployees.contains(v)
    }

    def addEmployees(v: LibrarySimple0#EmployeeRole): Boolean = {
      if (hasEmployees(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryEmployeesEmployeeRole(this, v)
      return true
    }

    def removeEmployees(v: LibrarySimple0#EmployeeRole): Boolean = {
      if (!hasEmployees(v)) return false
      employees.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Library(name)
    }

    override def toString(): String = {
      "VNR: LibraryRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

  }

  class EmployeeRole(protected val salary: Double, p_Name: String) extends PersonRole(p_Name) {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Employee(salary, name)
    }

    override def toString(): String = {
      "VNR: EmployeeRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getSalaryView(): Double = {
      +this getSalary ()
    }

    def setSalaryView(salary: Double): Unit = {
      +this setSalary (salary)
      +this changeSomething ()
    }

  }

  abstract class PersonRole(protected val name: String) extends AViewRole {

    override def toString(): String = {
      "VNR: PersonRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

  }

  class LibraryEmployeesEmployeeRole(private val source: LibrarySimple0#LibraryRole, private val target: LibrarySimple0#EmployeeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setEmployeesIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Employee = getPlayerOfType(target, "Employee").asInstanceOf[Employee]
      val v: LibraryEmployeesEmployee = new LibraryEmployeesEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeEmployeesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryEmployeesEmployeeRole"
    }

    def getSource(): LibrarySimple0#LibraryRole = {
      source
    }

    def getTarget(): LibrarySimple0#EmployeeRole = {
      target
    }

  }

}

object LibrarySimple0 extends IViewTypeInfo {

  override def getViewName(): String = "LibrarySimple0"

  def getJoinInfos(): Set[IJoinInfo] = Set.empty

  protected def getNewInstance(): IViewCompartment = new LibrarySimple0()

  def getNewView(): LibrarySimple0 = getNewViewTypeInstance().asInstanceOf[LibrarySimple0]
}
      

    