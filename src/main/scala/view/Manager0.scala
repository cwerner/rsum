package view

import org.rosi_project.model_management.sum.IViewCompartment
import lib.Employee
import lib.EmployeeManagerEmployee
import lib.Person

import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo

class Manager0 private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[Employee] || className.isInstanceOf[EmployeeManagerEmployee]) return true
    return false
  }

  def getViewName(): String = {
    Manager0.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[Employee]) return new EmployeeRole(0, null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[EmployeeManagerEmployee]) return new EmployeeManagerEmployeeRole(sourceRole.asInstanceOf[EmployeeRole], targetRole.asInstanceOf[EmployeeRole])
    return null
  }

  override def toString(): String = {
    "VC: Manager0"
  }

  def createEmployee(salary: Double, name: String): Manager0#EmployeeRole = {
    return new EmployeeRole(salary, name)
  }

  class EmployeeRole(protected val salary: Double, p_Name: String) extends PersonRole(p_Name) {

    private var manager: Manager0#EmployeeManagerEmployeeRole = null

    private[Manager0] def removeManagerIntern(v: Manager0#EmployeeManagerEmployeeRole): Unit = {
      manager = null
    }

    private[Manager0] def setManagerIntern(v: Manager0#EmployeeManagerEmployeeRole): Unit = {
      manager = v
    }

    def getManager(): Manager0#EmployeeRole = {
      return manager.getTarget()
    }

    def setManager(v: Manager0#EmployeeRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (manager != null) {
        if (manager.getTarget() == v) return false
        manager.deleteElement()
      }
      new EmployeeManagerEmployeeRole(this, v)
      return true
    }

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Employee(salary, name)
    }

    override def toString(): String = {
      "VNR: EmployeeRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getSalaryView(): Double = {
      +this getSalary ()
    }

    def setSalaryView(salary: Double): Unit = {
      +this setSalary (salary)
      +this changeSomething ()
    }

  }

  abstract class PersonRole(protected val name: String) extends AViewRole {

    override def toString(): String = {
      "VNR: PersonRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

  }

  class EmployeeManagerEmployeeRole(private val source: Manager0#EmployeeRole, private val target: Manager0#EmployeeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setManagerIntern(this)
      val sp: Employee = getPlayerOfType(source, "Employee").asInstanceOf[Employee]
      val tp: Employee = getPlayerOfType(target, "Employee").asInstanceOf[Employee]
      val v: EmployeeManagerEmployee = new EmployeeManagerEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeManagerIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: EmployeeManagerEmployeeRole"
    }

    def getSource(): Manager0#EmployeeRole = {
      source
    }

    def getTarget(): Manager0#EmployeeRole = {
      target
    }

  }

}

object Manager0 extends IViewTypeInfo {

  override def getViewName(): String = "Manager0"

  def getJoinInfos(): Set[IJoinInfo] = Set.empty

  protected def getNewInstance(): IViewCompartment = new Manager0()

  def getNewView(): Manager0 = getNewViewTypeInstance().asInstanceOf[Manager0]
}
      

    