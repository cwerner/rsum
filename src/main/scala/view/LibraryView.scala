package view

import org.rosi_project.model_management.sum.IViewCompartment
import lib.EmployeeManagerEmployee
import lib.Library
import lib.Person
import lib.LibraryEmployeesEmployee
import lib.Employee

import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo

class LibraryView private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[EmployeeManagerEmployee] || className.isInstanceOf[LibraryEmployeesEmployee] || className.isInstanceOf[Library] || className.isInstanceOf[Employee]) return true
    return false
  }

  def getViewName(): String = {
    LibraryView.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[Library]) return new LibraryRole(null)
    if (className.isInstanceOf[Employee]) return new EmployeeRole(0, null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[LibraryEmployeesEmployee]) return new LibraryEmployeesEmployeeRole(sourceRole.asInstanceOf[LibraryRole], targetRole.asInstanceOf[EmployeeRole])
    if (className.isInstanceOf[EmployeeManagerEmployee]) return new EmployeeManagerEmployeeRole(sourceRole.asInstanceOf[EmployeeRole], targetRole.asInstanceOf[EmployeeRole])
    return null
  }

  override def toString(): String = {
    "VC: LibraryView"
  }

  def createLibrary(name: String): LibraryView#LibraryRole = {
    return new LibraryRole(name)
  }

  def createEmployee(salary: Double, name: String): LibraryView#EmployeeRole = {
    return new EmployeeRole(salary, name)
  }

  class LibraryRole(protected val name: String) extends AViewRole {

    private var employees: Set[LibraryView#LibraryEmployeesEmployeeRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Library(name)
    }

    override def toString(): String = {
      "VNR: LibraryRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

    private[LibraryView] def removeEmployeesIntern(v: LibraryView#LibraryEmployeesEmployeeRole): Unit = {
      employees -= v
    }

    private[LibraryView] def setEmployeesIntern(v: LibraryView#LibraryEmployeesEmployeeRole): Unit = {
      employees += v
    }

    def getEmployees(): Set[LibraryView#EmployeeRole] = {
      var vs: Set[LibraryView#EmployeeRole] = Set.empty
      employees.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasEmployees(v: LibraryView#EmployeeRole): Boolean = {
      return getEmployees.contains(v)
    }

    def addEmployees(v: LibraryView#EmployeeRole): Boolean = {
      if (hasEmployees(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new LibraryEmployeesEmployeeRole(this, v)
      return true
    }

    def removeEmployees(v: LibraryView#EmployeeRole): Boolean = {
      if (!hasEmployees(v)) return false
      employees.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class EmployeeRole(protected val salary: Double, p_Name: String) extends PersonRole(p_Name) {

    private var manager: LibraryView#EmployeeManagerEmployeeRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Employee(salary, name)
    }

    override def toString(): String = {
      "VNR: EmployeeRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getSalaryView(): Double = {
      +this getSalary ()
    }

    def setSalaryView(salary: Double): Unit = {
      +this setSalary (salary)
      +this changeSomething ()
    }

    private[LibraryView] def removeManagerIntern(v: LibraryView#EmployeeManagerEmployeeRole): Unit = {
      manager = null
    }

    private[LibraryView] def setManagerIntern(v: LibraryView#EmployeeManagerEmployeeRole): Unit = {
      manager = v
    }

    def getManager(): LibraryView#EmployeeRole = {
      return manager.getTarget()
    }

    def setManager(v: LibraryView#EmployeeRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (manager != null) {
        if (manager.getTarget() == v) return false
        manager.deleteElement()
      }
      new EmployeeManagerEmployeeRole(this, v)
      return true
    }

  }

  abstract class PersonRole(protected val name: String) extends AViewRole {

    override def toString(): String = {
      "VNR: PersonRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

  }

  class LibraryEmployeesEmployeeRole(private val source: LibraryView#LibraryRole, private val target: LibraryView#EmployeeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setEmployeesIntern(this)
      val sp: Library = getPlayerOfType(source, "Library").asInstanceOf[Library]
      val tp: Employee = getPlayerOfType(target, "Employee").asInstanceOf[Employee]
      val v: LibraryEmployeesEmployee = new LibraryEmployeesEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeEmployeesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: LibraryEmployeesEmployeeRole"
    }

    def getSource(): LibraryView#LibraryRole = {
      source
    }

    def getTarget(): LibraryView#EmployeeRole = {
      target
    }

  }

  class EmployeeManagerEmployeeRole(private val source: LibraryView#EmployeeRole, private val target: LibraryView#EmployeeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setManagerIntern(this)
      val sp: Employee = getPlayerOfType(source, "Employee").asInstanceOf[Employee]
      val tp: Employee = getPlayerOfType(target, "Employee").asInstanceOf[Employee]
      val v: EmployeeManagerEmployee = new EmployeeManagerEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeManagerIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: EmployeeManagerEmployeeRole"
    }

    def getSource(): LibraryView#EmployeeRole = {
      source
    }

    def getTarget(): LibraryView#EmployeeRole = {
      target
    }

  }

}

object LibraryView extends IViewTypeInfo {

  override def getViewName(): String = "LibraryView"

  def getJoinInfos(): Set[IJoinInfo] = Set.empty

  protected def getNewInstance(): IViewCompartment = new LibraryView()

  def getNewView(): LibraryView = getNewViewTypeInstance().asInstanceOf[LibraryView]
}
      

    