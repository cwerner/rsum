package view

import org.rosi_project.model_management.sum.IViewCompartment
import aml.SystemUnitClassInternalElementsInternalElement
import aml.InternalElement
import aml.SystemUnitClassAttributesAttribute
import aml.CAEXObject
import aml.Attribute
import aml.InstanceHierarchyInternalElementsInternalElement
import aml.InternalElementBaseSystemUnitSystemUnitClass
import aml.InstanceHierarchy
import aml.SystemUnitClass
import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo

class AMLLanguageView private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[SystemUnitClassInternalElementsInternalElement] || className.isInstanceOf[InternalElementBaseSystemUnitSystemUnitClass] || className.isInstanceOf[InstanceHierarchyInternalElementsInternalElement] || className.isInstanceOf[SystemUnitClass] || className.isInstanceOf[SystemUnitClassAttributesAttribute] || className.isInstanceOf[InstanceHierarchy] || className.isInstanceOf[Attribute] || className.isInstanceOf[InternalElement]) return true
    return false
  }

  def getViewName(): String = {
    AMLLanguageView.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[SystemUnitClass]) return new SystemUnitClassRole(null, null)
    if (className.isInstanceOf[InternalElement]) return new InternalElementRole(null, null)
    if (className.isInstanceOf[Attribute]) return new AttributeRole(null, null, null)
    if (className.isInstanceOf[InstanceHierarchy]) return new InstanceHierarchyRole(null, null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[SystemUnitClassInternalElementsInternalElement]) return new SystemUnitClassInternalElementsInternalElementRole(sourceRole.asInstanceOf[SystemUnitClassRole], targetRole.asInstanceOf[InternalElementRole])
    if (className.isInstanceOf[SystemUnitClassAttributesAttribute]) return new SystemUnitClassAttributesAttributeRole(sourceRole.asInstanceOf[SystemUnitClassRole], targetRole.asInstanceOf[AttributeRole])
    if (className.isInstanceOf[InstanceHierarchyInternalElementsInternalElement]) return new InstanceHierarchyInternalElementsInternalElementRole(sourceRole.asInstanceOf[InstanceHierarchyRole], targetRole.asInstanceOf[InternalElementRole])
    if (className.isInstanceOf[InternalElementBaseSystemUnitSystemUnitClass]) return new InternalElementBaseSystemUnitSystemUnitClassRole(sourceRole.asInstanceOf[InternalElementRole], targetRole.asInstanceOf[SystemUnitClassRole])
    return null
  }

  override def toString(): String = {
    "VC: AMLLanguageView"
  }

  def createSystemUnitClass(name: String, id: String): AMLLanguageView#SystemUnitClassRole = {
    return new SystemUnitClassRole(name, id)
  }

  def createInternalElement(name: String, id: String): AMLLanguageView#InternalElementRole = {
    return new InternalElementRole(name, id)
  }

  def createAttribute(value: String, name: String, id: String): AMLLanguageView#AttributeRole = {
    return new AttributeRole(value, name, id)
  }

  def createInstanceHierarchy(name: String, id: String): AMLLanguageView#InstanceHierarchyRole = {
    return new InstanceHierarchyRole(name, id)
  }

  class SystemUnitClassRole(c_Name: String, c_Id: String) extends CAEXObjectRole(c_Name, c_Id) {

    private var internalElements: Set[AMLLanguageView#SystemUnitClassInternalElementsInternalElementRole] = Set.empty
    private var attributes: Set[AMLLanguageView#SystemUnitClassAttributesAttributeRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new SystemUnitClass(name, id)
    }

    override def toString(): String = {
      "VNR: SystemUnitClassRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[AMLLanguageView] def removeInternalElementsIntern(v: AMLLanguageView#SystemUnitClassInternalElementsInternalElementRole): Unit = {
      internalElements -= v
    }

    private[AMLLanguageView] def setInternalElementsIntern(v: AMLLanguageView#SystemUnitClassInternalElementsInternalElementRole): Unit = {
      internalElements += v
    }

    def getInternalElements(): Set[AMLLanguageView#InternalElementRole] = {
      var vs: Set[AMLLanguageView#InternalElementRole] = Set.empty
      internalElements.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasInternalElements(v: AMLLanguageView#InternalElementRole): Boolean = {
      return getInternalElements.contains(v)
    }

    def addInternalElements(v: AMLLanguageView#InternalElementRole): Boolean = {
      if (hasInternalElements(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new SystemUnitClassInternalElementsInternalElementRole(this, v)
      return true
    }

    def removeInternalElements(v: AMLLanguageView#InternalElementRole): Boolean = {
      if (!hasInternalElements(v)) return false
      internalElements.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[AMLLanguageView] def removeAttributesIntern(v: AMLLanguageView#SystemUnitClassAttributesAttributeRole): Unit = {
      attributes -= v
    }

    private[AMLLanguageView] def setAttributesIntern(v: AMLLanguageView#SystemUnitClassAttributesAttributeRole): Unit = {
      attributes += v
    }

    def getAttributes(): Set[AMLLanguageView#AttributeRole] = {
      var vs: Set[AMLLanguageView#AttributeRole] = Set.empty
      attributes.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasAttributes(v: AMLLanguageView#AttributeRole): Boolean = {
      return getAttributes.contains(v)
    }

    def addAttributes(v: AMLLanguageView#AttributeRole): Boolean = {
      if (hasAttributes(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new SystemUnitClassAttributesAttributeRole(this, v)
      return true
    }

    def removeAttributes(v: AMLLanguageView#AttributeRole): Boolean = {
      if (!hasAttributes(v)) return false
      attributes.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class AttributeRole(protected val value: String, c_Name: String, c_Id: String) extends CAEXObjectRole(c_Name, c_Id) {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Attribute(value, name, id)
    }

    override def toString(): String = {
      "VNR: AttributeRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getValueView(): String = {
      +this getValue ()
    }

    def setValueView(value: String): Unit = {
      +this setValue (value)
      +this changeSomething ()
    }

  }

  class InstanceHierarchyRole(c_Name: String, c_Id: String) extends CAEXObjectRole(c_Name, c_Id) {

    private var internalElements: Set[AMLLanguageView#InstanceHierarchyInternalElementsInternalElementRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new InstanceHierarchy(name, id)
    }

    override def toString(): String = {
      "VNR: InstanceHierarchyRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[AMLLanguageView] def removeInternalElementsIntern(v: AMLLanguageView#InstanceHierarchyInternalElementsInternalElementRole): Unit = {
      internalElements -= v
    }

    private[AMLLanguageView] def setInternalElementsIntern(v: AMLLanguageView#InstanceHierarchyInternalElementsInternalElementRole): Unit = {
      internalElements += v
    }

    def getInternalElements(): Set[AMLLanguageView#InternalElementRole] = {
      var vs: Set[AMLLanguageView#InternalElementRole] = Set.empty
      internalElements.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasInternalElements(v: AMLLanguageView#InternalElementRole): Boolean = {
      return getInternalElements.contains(v)
    }

    def addInternalElements(v: AMLLanguageView#InternalElementRole): Boolean = {
      if (hasInternalElements(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new InstanceHierarchyInternalElementsInternalElementRole(this, v)
      return true
    }

    def removeInternalElements(v: AMLLanguageView#InternalElementRole): Boolean = {
      if (!hasInternalElements(v)) return false
      internalElements.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  abstract class CAEXObjectRole(protected val name: String, protected val id: String) extends AViewRole {

    override def toString(): String = {
      "VNR: CAEXObjectRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

    def getIdView(): String = {
      +this getId ()
    }

    def setIdView(id: String): Unit = {
      +this setId (id)
      +this changeSomething ()
    }

  }

  class InternalElementRole(s_Name: String, s_Id: String) extends SystemUnitClassRole(s_Name, s_Id) {

    private var baseSystemUnit: AMLLanguageView#InternalElementBaseSystemUnitSystemUnitClassRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new InternalElement(name, id)
    }

    override def toString(): String = {
      "VNR: InternalElementRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[AMLLanguageView] def removeBaseSystemUnitIntern(v: AMLLanguageView#InternalElementBaseSystemUnitSystemUnitClassRole): Unit = {
      baseSystemUnit = null
    }

    private[AMLLanguageView] def setBaseSystemUnitIntern(v: AMLLanguageView#InternalElementBaseSystemUnitSystemUnitClassRole): Unit = {
      baseSystemUnit = v
    }

    def getBaseSystemUnit(): AMLLanguageView#SystemUnitClassRole = {
      return baseSystemUnit.getTarget()
    }

    def setBaseSystemUnit(v: AMLLanguageView#SystemUnitClassRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (baseSystemUnit != null) {
        if (baseSystemUnit.getTarget() == v) return false
        baseSystemUnit.deleteElement()
      }
      new InternalElementBaseSystemUnitSystemUnitClassRole(this, v)
      return true
    }

  }

  class SystemUnitClassInternalElementsInternalElementRole(private val source: AMLLanguageView#SystemUnitClassRole, private val target: AMLLanguageView#InternalElementRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setInternalElementsIntern(this)
      val sp: SystemUnitClass = getPlayerOfType(source, "SystemUnitClass").asInstanceOf[SystemUnitClass]
      val tp: InternalElement = getPlayerOfType(target, "InternalElement").asInstanceOf[InternalElement]
      val v: SystemUnitClassInternalElementsInternalElement = new SystemUnitClassInternalElementsInternalElement(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeInternalElementsIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: SystemUnitClassInternalElementsInternalElementRole"
    }

    def getSource(): AMLLanguageView#SystemUnitClassRole = {
      source
    }

    def getTarget(): AMLLanguageView#InternalElementRole = {
      target
    }

  }

  class SystemUnitClassAttributesAttributeRole(private val source: AMLLanguageView#SystemUnitClassRole, private val target: AMLLanguageView#AttributeRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setAttributesIntern(this)
      val sp: SystemUnitClass = getPlayerOfType(source, "SystemUnitClass").asInstanceOf[SystemUnitClass]
      val tp: Attribute = getPlayerOfType(target, "Attribute").asInstanceOf[Attribute]
      val v: SystemUnitClassAttributesAttribute = new SystemUnitClassAttributesAttribute(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeAttributesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: SystemUnitClassAttributesAttributeRole"
    }

    def getSource(): AMLLanguageView#SystemUnitClassRole = {
      source
    }

    def getTarget(): AMLLanguageView#AttributeRole = {
      target
    }

  }

  class InstanceHierarchyInternalElementsInternalElementRole(private val source: AMLLanguageView#InstanceHierarchyRole, private val target: AMLLanguageView#InternalElementRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setInternalElementsIntern(this)
      val sp: InstanceHierarchy = getPlayerOfType(source, "InstanceHierarchy").asInstanceOf[InstanceHierarchy]
      val tp: InternalElement = getPlayerOfType(target, "InternalElement").asInstanceOf[InternalElement]
      val v: InstanceHierarchyInternalElementsInternalElement = new InstanceHierarchyInternalElementsInternalElement(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeInternalElementsIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: InstanceHierarchyInternalElementsInternalElementRole"
    }

    def getSource(): AMLLanguageView#InstanceHierarchyRole = {
      source
    }

    def getTarget(): AMLLanguageView#InternalElementRole = {
      target
    }

  }

  class InternalElementBaseSystemUnitSystemUnitClassRole(private val source: AMLLanguageView#InternalElementRole, private val target: AMLLanguageView#SystemUnitClassRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBaseSystemUnitIntern(this)
      val sp: InternalElement = getPlayerOfType(source, "InternalElement").asInstanceOf[InternalElement]
      val tp: SystemUnitClass = getPlayerOfType(target, "SystemUnitClass").asInstanceOf[SystemUnitClass]
      val v: InternalElementBaseSystemUnitSystemUnitClass = new InternalElementBaseSystemUnitSystemUnitClass(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBaseSystemUnitIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: InternalElementBaseSystemUnitSystemUnitClassRole"
    }

    def getSource(): AMLLanguageView#InternalElementRole = {
      source
    }

    def getTarget(): AMLLanguageView#SystemUnitClassRole = {
      target
    }

  }

}

object AMLLanguageView extends IViewTypeInfo {

  override def getViewName(): String = "AMLLanguageView"

  def getJoinInfos(): Set[IJoinInfo] = Set.empty

  protected def getNewInstance(): IViewCompartment = new AMLLanguageView()

  def getNewView(): AMLLanguageView = getNewViewTypeInstance().asInstanceOf[AMLLanguageView]
}
     

    