package view

import org.rosi_project.model_management.sum.IViewCompartment
import imdbdatabase.User
import imdbdatabase.IMDBFilmsFilm
import imdbdatabase.FilmFiguresFigure
import imdbdatabase.VoteFilmFilm
import imdbdatabase.Film
import imdbdatabase.IMDBVotesVote
import imdbdatabase.VoteUserUser
import imdbdatabase.FigurePlayedByActor
import imdbdatabase.Person
import imdbdatabase.IMDB
import imdbdatabase.Figure
import imdbdatabase.Actor
import imdbdatabase.Vote
import imdbdatabase.IMDBUsersUser
import imdbdatabase.IMDBActorsActor

import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo

class IMDBdatabaseView private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[IMDBFilmsFilm] || className.isInstanceOf[IMDBVotesVote] || className.isInstanceOf[IMDBActorsActor] || className.isInstanceOf[FilmFiguresFigure] || className.isInstanceOf[Person] || className.isInstanceOf[IMDB] || className.isInstanceOf[Vote] || className.isInstanceOf[VoteUserUser] || className.isInstanceOf[FigurePlayedByActor] || className.isInstanceOf[User] || className.isInstanceOf[VoteFilmFilm] || className.isInstanceOf[IMDBUsersUser] || className.isInstanceOf[Actor] || className.isInstanceOf[Film] || className.isInstanceOf[Figure]) return true
    return false
  }

  def getViewName(): String = {
    IMDBdatabaseView.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[Figure]) return new FigureRole(null)
    if (className.isInstanceOf[Actor]) return new ActorRole(0, null)
    if (className.isInstanceOf[Person]) return new PersonRole(0, null)
    if (className.isInstanceOf[User]) return new UserRole(null, null, 0, null)
    if (className.isInstanceOf[Vote]) return new VoteRole(0)
    if (className.isInstanceOf[IMDB]) return new IMDBRole()
    if (className.isInstanceOf[Film]) return new FilmRole(0, null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[FilmFiguresFigure]) return new FilmFiguresFigureRole(sourceRole.asInstanceOf[FilmRole], targetRole.asInstanceOf[FigureRole])
    if (className.isInstanceOf[IMDBVotesVote]) return new IMDBVotesVoteRole(sourceRole.asInstanceOf[IMDBRole], targetRole.asInstanceOf[VoteRole])
    if (className.isInstanceOf[IMDBActorsActor]) return new IMDBActorsActorRole(sourceRole.asInstanceOf[IMDBRole], targetRole.asInstanceOf[ActorRole])
    if (className.isInstanceOf[IMDBUsersUser]) return new IMDBUsersUserRole(sourceRole.asInstanceOf[IMDBRole], targetRole.asInstanceOf[UserRole])
    if (className.isInstanceOf[VoteUserUser]) return new VoteUserUserRole(sourceRole.asInstanceOf[VoteRole], targetRole.asInstanceOf[UserRole])
    if (className.isInstanceOf[VoteFilmFilm]) return new VoteFilmFilmRole(sourceRole.asInstanceOf[VoteRole], targetRole.asInstanceOf[FilmRole])
    if (className.isInstanceOf[FigurePlayedByActor]) return new FigurePlayedByActorRole(sourceRole.asInstanceOf[FigureRole], targetRole.asInstanceOf[ActorRole])
    if (className.isInstanceOf[IMDBFilmsFilm]) return new IMDBFilmsFilmRole(sourceRole.asInstanceOf[IMDBRole], targetRole.asInstanceOf[FilmRole])
    return null
  }

  override def toString(): String = {
    "VC: IMDBdatabaseView"
  }

  def createFigure(name: String): IMDBdatabaseView#FigureRole = {
    return new FigureRole(name)
  }

  def createActor(dob: Double, name: String): IMDBdatabaseView#ActorRole = {
    return new ActorRole(dob, name)
  }

  def createPerson(dob: Double, name: String): IMDBdatabaseView#PersonRole = {
    return new PersonRole(dob, name)
  }

  def createUser(email: String, userName: String, dob: Double, name: String): IMDBdatabaseView#UserRole = {
    return new UserRole(email, userName, dob, name)
  }

  def createVote(score: Int): IMDBdatabaseView#VoteRole = {
    return new VoteRole(score)
  }

  def createIMDB(): IMDBdatabaseView#IMDBRole = {
    return new IMDBRole()
  }

  def createFilm(year: Int, title: String): IMDBdatabaseView#FilmRole = {
    return new FilmRole(year, title)
  }

  class IMDBRole extends AViewRole {

    private var users: Set[IMDBdatabaseView#IMDBUsersUserRole] = Set.empty
    private var votes: Set[IMDBdatabaseView#IMDBVotesVoteRole] = Set.empty
    private var actors: Set[IMDBdatabaseView#IMDBActorsActorRole] = Set.empty
    private var films: Set[IMDBdatabaseView#IMDBFilmsFilmRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new IMDB()
    }

    override def toString(): String = {
      "VNR: IMDBRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[IMDBdatabaseView] def removeUsersIntern(v: IMDBdatabaseView#IMDBUsersUserRole): Unit = {
      users -= v
    }

    private[IMDBdatabaseView] def setUsersIntern(v: IMDBdatabaseView#IMDBUsersUserRole): Unit = {
      users += v
    }

    def getUsers(): Set[IMDBdatabaseView#UserRole] = {
      var vs: Set[IMDBdatabaseView#UserRole] = Set.empty
      users.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasUsers(v: IMDBdatabaseView#UserRole): Boolean = {
      return getUsers.contains(v)
    }

    def addUsers(v: IMDBdatabaseView#UserRole): Boolean = {
      if (hasUsers(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new IMDBUsersUserRole(this, v)
      return true
    }

    def removeUsers(v: IMDBdatabaseView#UserRole): Boolean = {
      if (!hasUsers(v)) return false
      users.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseView] def removeVotesIntern(v: IMDBdatabaseView#IMDBVotesVoteRole): Unit = {
      votes -= v
    }

    private[IMDBdatabaseView] def setVotesIntern(v: IMDBdatabaseView#IMDBVotesVoteRole): Unit = {
      votes += v
    }

    def getVotes(): Set[IMDBdatabaseView#VoteRole] = {
      var vs: Set[IMDBdatabaseView#VoteRole] = Set.empty
      votes.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasVotes(v: IMDBdatabaseView#VoteRole): Boolean = {
      return getVotes.contains(v)
    }

    def addVotes(v: IMDBdatabaseView#VoteRole): Boolean = {
      if (hasVotes(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new IMDBVotesVoteRole(this, v)
      return true
    }

    def removeVotes(v: IMDBdatabaseView#VoteRole): Boolean = {
      if (!hasVotes(v)) return false
      votes.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseView] def removeActorsIntern(v: IMDBdatabaseView#IMDBActorsActorRole): Unit = {
      actors -= v
    }

    private[IMDBdatabaseView] def setActorsIntern(v: IMDBdatabaseView#IMDBActorsActorRole): Unit = {
      actors += v
    }

    def getActors(): Set[IMDBdatabaseView#ActorRole] = {
      var vs: Set[IMDBdatabaseView#ActorRole] = Set.empty
      actors.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasActors(v: IMDBdatabaseView#ActorRole): Boolean = {
      return getActors.contains(v)
    }

    def addActors(v: IMDBdatabaseView#ActorRole): Boolean = {
      if (hasActors(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new IMDBActorsActorRole(this, v)
      return true
    }

    def removeActors(v: IMDBdatabaseView#ActorRole): Boolean = {
      if (!hasActors(v)) return false
      actors.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseView] def removeFilmsIntern(v: IMDBdatabaseView#IMDBFilmsFilmRole): Unit = {
      films -= v
    }

    private[IMDBdatabaseView] def setFilmsIntern(v: IMDBdatabaseView#IMDBFilmsFilmRole): Unit = {
      films += v
    }

    def getFilms(): Set[IMDBdatabaseView#FilmRole] = {
      var vs: Set[IMDBdatabaseView#FilmRole] = Set.empty
      films.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasFilms(v: IMDBdatabaseView#FilmRole): Boolean = {
      return getFilms.contains(v)
    }

    def addFilms(v: IMDBdatabaseView#FilmRole): Boolean = {
      if (hasFilms(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new IMDBFilmsFilmRole(this, v)
      return true
    }

    def removeFilms(v: IMDBdatabaseView#FilmRole): Boolean = {
      if (!hasFilms(v)) return false
      films.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class FilmRole(protected val year: Int, protected val title: String) extends AViewRole {

    private var library: IMDBdatabaseView#IMDBFilmsFilmRole = null
    private var votes: Set[IMDBdatabaseView#VoteFilmFilmRole] = Set.empty
    private var figures: Set[IMDBdatabaseView#FilmFiguresFigureRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Film(year, title)
    }

    override def toString(): String = {
      "VNR: FilmRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getYearView(): Int = {
      +this getYear ()
    }

    def setYearView(year: Int): Unit = {
      +this setYear (year)
      +this changeSomething ()
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String): Unit = {
      +this setTitle (title)
      +this changeSomething ()
    }

    private[IMDBdatabaseView] def removeLibraryIntern(v: IMDBdatabaseView#IMDBFilmsFilmRole): Unit = {
      library = null
    }

    private[IMDBdatabaseView] def setLibraryIntern(v: IMDBdatabaseView#IMDBFilmsFilmRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseView#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseView#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBFilmsFilmRole(v, this)
      return true
    }

    private[IMDBdatabaseView] def removeVotesIntern(v: IMDBdatabaseView#VoteFilmFilmRole): Unit = {
      votes -= v
    }

    private[IMDBdatabaseView] def setVotesIntern(v: IMDBdatabaseView#VoteFilmFilmRole): Unit = {
      votes += v
    }

    def getVotes(): Set[IMDBdatabaseView#VoteRole] = {
      var vs: Set[IMDBdatabaseView#VoteRole] = Set.empty
      votes.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasVotes(v: IMDBdatabaseView#VoteRole): Boolean = {
      return getVotes.contains(v)
    }

    def addVotes(v: IMDBdatabaseView#VoteRole): Boolean = {
      if (hasVotes(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new VoteFilmFilmRole(v, this)
      return true
    }

    def removeVotes(v: IMDBdatabaseView#VoteRole): Boolean = {
      if (!hasVotes(v)) return false
      votes.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseView] def removeFiguresIntern(v: IMDBdatabaseView#FilmFiguresFigureRole): Unit = {
      figures -= v
    }

    private[IMDBdatabaseView] def setFiguresIntern(v: IMDBdatabaseView#FilmFiguresFigureRole): Unit = {
      figures += v
    }

    def getFigures(): Set[IMDBdatabaseView#FigureRole] = {
      var vs: Set[IMDBdatabaseView#FigureRole] = Set.empty
      figures.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasFigures(v: IMDBdatabaseView#FigureRole): Boolean = {
      return getFigures.contains(v)
    }

    def addFigures(v: IMDBdatabaseView#FigureRole): Boolean = {
      if (hasFigures(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new FilmFiguresFigureRole(this, v)
      return true
    }

    def removeFigures(v: IMDBdatabaseView#FigureRole): Boolean = {
      if (!hasFigures(v)) return false
      figures.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

  }

  class VoteRole(protected val score: Int) extends AViewRole {

    private var user: IMDBdatabaseView#VoteUserUserRole = null
    private var library: IMDBdatabaseView#IMDBVotesVoteRole = null
    private var film: IMDBdatabaseView#VoteFilmFilmRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Vote(score)
    }

    override def toString(): String = {
      "VNR: VoteRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getScoreView(): Int = {
      +this getScore ()
    }

    def setScoreView(score: Int): Unit = {
      +this setScore (score)
      +this changeSomething ()
    }

    private[IMDBdatabaseView] def removeUserIntern(v: IMDBdatabaseView#VoteUserUserRole): Unit = {
      user = null
    }

    private[IMDBdatabaseView] def setUserIntern(v: IMDBdatabaseView#VoteUserUserRole): Unit = {
      user = v
    }

    def getUser(): IMDBdatabaseView#UserRole = {
      return user.getTarget()
    }

    def setUser(v: IMDBdatabaseView#UserRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (user != null) {
        if (user.getTarget() == v) return false
        user.deleteElement()
      }
      new VoteUserUserRole(this, v)
      return true
    }

    private[IMDBdatabaseView] def removeLibraryIntern(v: IMDBdatabaseView#IMDBVotesVoteRole): Unit = {
      library = null
    }

    private[IMDBdatabaseView] def setLibraryIntern(v: IMDBdatabaseView#IMDBVotesVoteRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseView#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseView#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBVotesVoteRole(v, this)
      return true
    }

    private[IMDBdatabaseView] def removeFilmIntern(v: IMDBdatabaseView#VoteFilmFilmRole): Unit = {
      film = null
    }

    private[IMDBdatabaseView] def setFilmIntern(v: IMDBdatabaseView#VoteFilmFilmRole): Unit = {
      film = v
    }

    def getFilm(): IMDBdatabaseView#FilmRole = {
      return film.getTarget()
    }

    def setFilm(v: IMDBdatabaseView#FilmRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (film != null) {
        if (film.getTarget() == v) return false
        film.deleteElement()
      }
      new VoteFilmFilmRole(this, v)
      return true
    }

  }

  class ActorRole(p_Dob: Double, p_Name: String) extends PersonRole(p_Dob, p_Name) {

    private var plays: Set[IMDBdatabaseView#FigurePlayedByActorRole] = Set.empty
    private var library: IMDBdatabaseView#IMDBActorsActorRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Actor(dob, name)
    }

    override def toString(): String = {
      "VNR: ActorRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    private[IMDBdatabaseView] def removePlaysIntern(v: IMDBdatabaseView#FigurePlayedByActorRole): Unit = {
      plays -= v
    }

    private[IMDBdatabaseView] def setPlaysIntern(v: IMDBdatabaseView#FigurePlayedByActorRole): Unit = {
      plays += v
    }

    def getPlays(): Set[IMDBdatabaseView#FigureRole] = {
      var vs: Set[IMDBdatabaseView#FigureRole] = Set.empty
      plays.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasPlays(v: IMDBdatabaseView#FigureRole): Boolean = {
      return getPlays.contains(v)
    }

    def addPlays(v: IMDBdatabaseView#FigureRole): Boolean = {
      if (hasPlays(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new FigurePlayedByActorRole(v, this)
      return true
    }

    def removePlays(v: IMDBdatabaseView#FigureRole): Boolean = {
      if (!hasPlays(v)) return false
      plays.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseView] def removeLibraryIntern(v: IMDBdatabaseView#IMDBActorsActorRole): Unit = {
      library = null
    }

    private[IMDBdatabaseView] def setLibraryIntern(v: IMDBdatabaseView#IMDBActorsActorRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseView#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseView#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBActorsActorRole(v, this)
      return true
    }

  }

  class PersonRole(protected val dob: Double, protected val name: String) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Person(dob, name)
    }

    override def toString(): String = {
      "VNR: PersonRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getDobView(): Double = {
      +this getDob ()
    }

    def setDobView(dob: Double): Unit = {
      +this setDob (dob)
      +this changeSomething ()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

  }

  class FigureRole(protected val name: String) extends AViewRole {

    private var playedBy: Set[IMDBdatabaseView#FigurePlayedByActorRole] = Set.empty
    private var film: IMDBdatabaseView#FilmFiguresFigureRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Figure(name)
    }

    override def toString(): String = {
      "VNR: FigureRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String): Unit = {
      +this setName (name)
      +this changeSomething ()
    }

    private[IMDBdatabaseView] def removePlayedByIntern(v: IMDBdatabaseView#FigurePlayedByActorRole): Unit = {
      playedBy -= v
    }

    private[IMDBdatabaseView] def setPlayedByIntern(v: IMDBdatabaseView#FigurePlayedByActorRole): Unit = {
      playedBy += v
    }

    def getPlayedBy(): Set[IMDBdatabaseView#ActorRole] = {
      var vs: Set[IMDBdatabaseView#ActorRole] = Set.empty
      playedBy.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasPlayedBy(v: IMDBdatabaseView#ActorRole): Boolean = {
      return getPlayedBy.contains(v)
    }

    def addPlayedBy(v: IMDBdatabaseView#ActorRole): Boolean = {
      if (hasPlayedBy(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new FigurePlayedByActorRole(this, v)
      return true
    }

    def removePlayedBy(v: IMDBdatabaseView#ActorRole): Boolean = {
      if (!hasPlayedBy(v)) return false
      playedBy.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseView] def removeFilmIntern(v: IMDBdatabaseView#FilmFiguresFigureRole): Unit = {
      film = null
    }

    private[IMDBdatabaseView] def setFilmIntern(v: IMDBdatabaseView#FilmFiguresFigureRole): Unit = {
      film = v
    }

    def getFilm(): IMDBdatabaseView#FilmRole = {
      return film.getSource()
    }

    def setFilm(v: IMDBdatabaseView#FilmRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (film != null) {
        if (film.getSource() == v) return false
        film.deleteElement()
      }
      new FilmFiguresFigureRole(v, this)
      return true
    }

  }

  class UserRole(protected val email: String, protected val userName: String, p_Dob: Double, p_Name: String) extends PersonRole(p_Dob, p_Name) {

    private var library: IMDBdatabaseView#IMDBUsersUserRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new User(email, userName, dob, name)
    }

    override def toString(): String = {
      "VNR: UserRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getEmailView(): String = {
      +this getEmail ()
    }

    def setEmailView(email: String): Unit = {
      +this setEmail (email)
      +this changeSomething ()
    }

    def getUserNameView(): String = {
      +this getUserName ()
    }

    def setUserNameView(username: String): Unit = {
      +this setUserName (username)
      +this changeSomething ()
    }

    private[IMDBdatabaseView] def removeLibraryIntern(v: IMDBdatabaseView#IMDBUsersUserRole): Unit = {
      library = null
    }

    private[IMDBdatabaseView] def setLibraryIntern(v: IMDBdatabaseView#IMDBUsersUserRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseView#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseView#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[AViewRole])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBUsersUserRole(v, this)
      return true
    }

  }

  class FigurePlayedByActorRole(private val source: IMDBdatabaseView#FigureRole, private val target: IMDBdatabaseView#ActorRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setPlaysIntern(this)
      source.setPlayedByIntern(this)
      val sp: Figure = getPlayerOfType(source, "Figure").asInstanceOf[Figure]
      val tp: Actor = getPlayerOfType(target, "Actor").asInstanceOf[Actor]
      val v: FigurePlayedByActor = new FigurePlayedByActor(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removePlayedByIntern(this)
      target.removePlaysIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: FigurePlayedByActorRole"
    }

    def getSource(): IMDBdatabaseView#FigureRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#ActorRole = {
      target
    }

  }

  class VoteUserUserRole(private val source: IMDBdatabaseView#VoteRole, private val target: IMDBdatabaseView#UserRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setUserIntern(this)
      val sp: Vote = getPlayerOfType(source, "Vote").asInstanceOf[Vote]
      val tp: User = getPlayerOfType(target, "User").asInstanceOf[User]
      val v: VoteUserUser = new VoteUserUser(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeUserIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: VoteUserUserRole"
    }

    def getSource(): IMDBdatabaseView#VoteRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#UserRole = {
      target
    }

  }

  class IMDBUsersUserRole(private val source: IMDBdatabaseView#IMDBRole, private val target: IMDBdatabaseView#UserRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setUsersIntern(this)
      val sp: IMDB = getPlayerOfType(source, "IMDB").asInstanceOf[IMDB]
      val tp: User = getPlayerOfType(target, "User").asInstanceOf[User]
      val v: IMDBUsersUser = new IMDBUsersUser(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeUsersIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: IMDBUsersUserRole"
    }

    def getSource(): IMDBdatabaseView#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#UserRole = {
      target
    }

  }

  class IMDBVotesVoteRole(private val source: IMDBdatabaseView#IMDBRole, private val target: IMDBdatabaseView#VoteRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setVotesIntern(this)
      val sp: IMDB = getPlayerOfType(source, "IMDB").asInstanceOf[IMDB]
      val tp: Vote = getPlayerOfType(target, "Vote").asInstanceOf[Vote]
      val v: IMDBVotesVote = new IMDBVotesVote(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeVotesIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: IMDBVotesVoteRole"
    }

    def getSource(): IMDBdatabaseView#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#VoteRole = {
      target
    }

  }

  class IMDBActorsActorRole(private val source: IMDBdatabaseView#IMDBRole, private val target: IMDBdatabaseView#ActorRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setActorsIntern(this)
      val sp: IMDB = getPlayerOfType(source, "IMDB").asInstanceOf[IMDB]
      val tp: Actor = getPlayerOfType(target, "Actor").asInstanceOf[Actor]
      val v: IMDBActorsActor = new IMDBActorsActor(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeActorsIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: IMDBActorsActorRole"
    }

    def getSource(): IMDBdatabaseView#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#ActorRole = {
      target
    }

  }

  class IMDBFilmsFilmRole(private val source: IMDBdatabaseView#IMDBRole, private val target: IMDBdatabaseView#FilmRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setFilmsIntern(this)
      val sp: IMDB = getPlayerOfType(source, "IMDB").asInstanceOf[IMDB]
      val tp: Film = getPlayerOfType(target, "Film").asInstanceOf[Film]
      val v: IMDBFilmsFilm = new IMDBFilmsFilm(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeFilmsIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: IMDBFilmsFilmRole"
    }

    def getSource(): IMDBdatabaseView#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#FilmRole = {
      target
    }

  }

  class VoteFilmFilmRole(private val source: IMDBdatabaseView#VoteRole, private val target: IMDBdatabaseView#FilmRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setVotesIntern(this)
      source.setFilmIntern(this)
      val sp: Vote = getPlayerOfType(source, "Vote").asInstanceOf[Vote]
      val tp: Film = getPlayerOfType(target, "Film").asInstanceOf[Film]
      val v: VoteFilmFilm = new VoteFilmFilm(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeFilmIntern(this)
      target.removeVotesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: VoteFilmFilmRole"
    }

    def getSource(): IMDBdatabaseView#VoteRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#FilmRole = {
      target
    }

  }

  class FilmFiguresFigureRole(private val source: IMDBdatabaseView#FilmRole, private val target: IMDBdatabaseView#FigureRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setFilmIntern(this)
      source.setFiguresIntern(this)
      val sp: Film = getPlayerOfType(source, "Film").asInstanceOf[Film]
      val tp: Figure = getPlayerOfType(target, "Figure").asInstanceOf[Figure]
      val v: FilmFiguresFigure = new FilmFiguresFigure(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeFiguresIntern(this)
      target.removeFilmIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: FilmFiguresFigureRole"
    }

    def getSource(): IMDBdatabaseView#FilmRole = {
      source
    }

    def getTarget(): IMDBdatabaseView#FigureRole = {
      target
    }

  }

}

object IMDBdatabaseView extends IViewTypeInfo {

  override def getViewName(): String = "IMDBdatabaseView"

  def getJoinInfos(): Set[IJoinInfo] = Set.empty

  protected def getNewInstance(): IViewCompartment = new IMDBdatabaseView()

  def getNewView(): IMDBdatabaseView = getNewViewTypeInstance().asInstanceOf[IMDBdatabaseView]
}
      

    