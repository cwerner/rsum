package view

import org.rosi_project.model_management.sum.IViewCompartment
import imdbdatabase.VoteFilmFilm
import imdbdatabase.Film
import elib.VideoCassette
import joins.JoinMovie
import elib.Person
import elib.VideoCassetteCastPerson
import imdbdatabase.Vote

import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.join.IJoinInfo
import joins.JoinMovieObject

class Simple0 private extends IViewCompartment {

  protected def isViewable(className: Object): Boolean = {
    if (className.isInstanceOf[Person] || className.isInstanceOf[Vote] || className.isInstanceOf[JoinMovie] || className.isInstanceOf[VoteFilmFilm] || className.isInstanceOf[VideoCassetteCastPerson]) return true
    return false
  }

  def getViewName(): String = {
    Simple0.getViewName()
  }

  protected def getNaturalRole(className: Object): AViewRole = {
    if (className.isInstanceOf[JoinMovie]) return new JoinMovieRole(null, 0, 0)
    if (className.isInstanceOf[Vote]) return new VoteRole(0)
    if (className.isInstanceOf[Person]) return new PersonRole(null)
    return null
  }

  protected def getRelationalRole(className: Object, sourceRole: AViewRole, targetRole: AViewRole): AViewRole = {
    if (className.isInstanceOf[VoteFilmFilm]) return new VoteFilmFilmRole(sourceRole.asInstanceOf[VoteRole], targetRole.asInstanceOf[JoinMovieRole])
    if (className.isInstanceOf[VideoCassetteCastPerson]) return new VideoCassetteCastPersonRole(sourceRole.asInstanceOf[JoinMovieRole], targetRole.asInstanceOf[PersonRole])
    return null
  }

  override def toString(): String = {
    "VC: Simple0"
  }

  def createJoinMovie(title: String, year: Int, minutesLength: Int): Simple0#JoinMovieRole = {
    return new JoinMovieRole(title, year, minutesLength)
  }

  def createVote(score: Int): Simple0#VoteRole = {
    return new VoteRole(score)
  }

  def createPerson(lastName: String): Simple0#PersonRole = {
    return new PersonRole(lastName)
  }

  class JoinMovieRole(protected val title: String, protected val year: Int, protected val minutesLength: Int) extends AViewRole {

    private var votes: Set[Simple0#VoteFilmFilmRole] = Set.empty
    private var cast: Set[Simple0#VideoCassetteCastPersonRole] = Set.empty

    private[Simple0] def removeVotesIntern(v: Simple0#VoteFilmFilmRole): Unit = {
      votes -= v
    }

    private[Simple0] def setVotesIntern(v: Simple0#VoteFilmFilmRole): Unit = {
      votes += v
    }

    def getVotes(): Set[Simple0#VoteRole] = {
      var vs: Set[Simple0#VoteRole] = Set.empty
      votes.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasVotes(v: Simple0#VoteRole): Boolean = {
      return getVotes.contains(v)
    }

    def addVotes(v: Simple0#VoteRole): Boolean = {
      if (hasVotes(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new VoteFilmFilmRole(v, this)
      return true
    }

    def removeVotes(v: Simple0#VoteRole): Boolean = {
      if (!hasVotes(v)) return false
      votes.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[Simple0] def removeCastIntern(v: Simple0#VideoCassetteCastPersonRole): Unit = {
      cast -= v
    }

    private[Simple0] def setCastIntern(v: Simple0#VideoCassetteCastPersonRole): Unit = {
      cast += v
    }

    def getCast(): Set[Simple0#PersonRole] = {
      var vs: Set[Simple0#PersonRole] = Set.empty
      cast.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasCast(v: Simple0#PersonRole): Boolean = {
      return getCast.contains(v)
    }

    def addCast(v: Simple0#PersonRole): Boolean = {
      if (hasCast(v) || !containsRole(v.asInstanceOf[AViewRole])) return false
      new VideoCassetteCastPersonRole(this, v)
      return true
    }

    def removeCast(v: Simple0#PersonRole): Boolean = {
      if (!hasCast(v)) return false
      cast.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new JoinMovie(new Film(year, title), new VideoCassette(false, minutesLength, title, null))
    }

    override def toString(): String = {
      "VNR: JoinMovieRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String): Unit = {
      +this setTitle (title)
      +this changeSomething ()
    }

    def getYearView(): Int = {
      +this getYear ()
    }

    def setYearView(year: Int): Unit = {
      +this setYear (year)
      +this changeSomething ()
    }

    def getMinutesLengthView(): Int = {
      +this getMinutesLength ()
    }

    def setMinutesLengthView(minuteslength: Int): Unit = {
      +this setMinutesLength (minuteslength)
      +this changeSomething ()
    }

  }

  class VoteRole(protected val score: Int) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Vote(score)
    }

    override def toString(): String = {
      "VNR: VoteRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getScoreView(): Int = {
      +this getScore ()
    }

    def setScoreView(score: Int): Unit = {
      +this setScore (score)
      +this changeSomething ()
    }

  }

  class VoteFilmFilmRole(private val source: Simple0#VoteRole, private val target: Simple0#JoinMovieRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setVotesIntern(this)
      val sp: Vote = getPlayerOfType(source, "Vote").asInstanceOf[Vote]
      val tp: Film = getPlayerOfType(target, "Film").asInstanceOf[Film]
      val v: VoteFilmFilm = new VoteFilmFilm(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      target.removeVotesIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: VoteFilmFilmRole"
    }

    def getSource(): Simple0#VoteRole = {
      source
    }

    def getTarget(): Simple0#JoinMovieRole = {
      target
    }

  }

  class PersonRole(protected val lastName: String) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Person(lastName, null)
    }

    override def toString(): String = {
      "VNR: PersonRole"
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getLastNameView(): String = {
      +this getLastName ()
    }

    def setLastNameView(lastname: String): Unit = {
      +this setLastName (lastname)
      +this changeSomething ()
    }

  }

  class VideoCassetteCastPersonRole(private val source: Simple0#JoinMovieRole, private val target: Simple0#PersonRole) extends AViewRole {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setCastIntern(this)
      val sp: VideoCassette = getPlayerOfType(source, "VideoCassette").asInstanceOf[VideoCassette]
      val tp: Person = getPlayerOfType(target, "Person").asInstanceOf[Person]
      val v: VideoCassetteCastPerson = new VideoCassetteCastPerson(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeCastIntern(this)
      super.deleteElement()
    }

    override def toString(): String = {
      "VRR: VideoCassetteCastPersonRole"
    }

    def getSource(): Simple0#JoinMovieRole = {
      source
    }

    def getTarget(): Simple0#PersonRole = {
      target
    }

  }

}

object Simple0 extends IViewTypeInfo {

  override def getViewName(): String = "Simple0"

  def getJoinInfos(): Set[IJoinInfo] = Set(JoinMovieObject)

  protected def getNewInstance(): IViewCompartment = new Simple0()

  def getNewView(): Simple0 = getNewViewTypeInstance().asInstanceOf[Simple0]
}
      

    