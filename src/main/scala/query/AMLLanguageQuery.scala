package query

import aml.SystemUnitClassInternalElementsInternalElement
import aml.InternalElement
import aml.SystemUnitClassAttributesAttribute
import aml.CAEXObject
import aml.Attribute
import org.rosi_project.model_management.sum.query.QueryFactory
import aml.InstanceHierarchyInternalElementsInternalElement
import aml.InternalElementBaseSystemUnitSystemUnitClass
import aml.HelperCAEXObject
import aml.InstanceHierarchy
import org.rosi_project.model_management.sum.query.CheckingOption
import aml.SystemUnitClass

class AMLLanguageQuery extends QueryFactory {

  init("AMLLanguageQuery")

  def createSystemUnitClass(): AMLLanguageQuery#SystemUnitClassRole = {
    return new SystemUnitClassRole()
  }

  def createInternalElement(): AMLLanguageQuery#InternalElementRole = {
    return new InternalElementRole()
  }

  def createCAEXObject(): AMLLanguageQuery#CAEXObjectRole = {
    return new CAEXObjectRole()
  }

  def createAttribute(): AMLLanguageQuery#AttributeRole = {
    return new AttributeRole()
  }

  def createInstanceHierarchy(): AMLLanguageQuery#InstanceHierarchyRole = {
    return new InstanceHierarchyRole()
  }

  override def toString(): String = {
    "AMLLanguageQuery:"
  }

  class SystemUnitClassRole extends CAEXObjectRole() {

    private var internalElements: Set[AMLLanguageQuery#SystemUnitClassInternalElementsInternalElementRole] = Set.empty
    private var attributes: Set[AMLLanguageQuery#SystemUnitClassAttributesAttributeRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new SystemUnitClass(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      internalElements.foreach(_.deleteElement())
      attributes.foreach(_.deleteElement())
    }

    private[AMLLanguageQuery] def removeInternalElementsIntern(v: AMLLanguageQuery#SystemUnitClassInternalElementsInternalElementRole): Unit = {
      internalElements -= v
    }

    private[AMLLanguageQuery] def setInternalElementsIntern(v: AMLLanguageQuery#SystemUnitClassInternalElementsInternalElementRole): Unit = {
      internalElements += v
    }

    def getInternalElements(): Set[AMLLanguageQuery#InternalElementRole] = {
      var vs: Set[AMLLanguageQuery#InternalElementRole] = Set.empty
      internalElements.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasInternalElements(v: AMLLanguageQuery#InternalElementRole): Boolean = {
      return getInternalElements.contains(v)
    }

    def addInternalElements(v: AMLLanguageQuery#InternalElementRole): Boolean = {
      if (hasInternalElements(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new SystemUnitClassInternalElementsInternalElementRole(this, v)
      return true
    }

    def removeInternalElements(v: AMLLanguageQuery#InternalElementRole): Boolean = {
      if (!hasInternalElements(v)) return false
      internalElements.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[AMLLanguageQuery] def removeAttributesIntern(v: AMLLanguageQuery#SystemUnitClassAttributesAttributeRole): Unit = {
      attributes -= v
    }

    private[AMLLanguageQuery] def setAttributesIntern(v: AMLLanguageQuery#SystemUnitClassAttributesAttributeRole): Unit = {
      attributes += v
    }

    def getAttributes(): Set[AMLLanguageQuery#AttributeRole] = {
      var vs: Set[AMLLanguageQuery#AttributeRole] = Set.empty
      attributes.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasAttributes(v: AMLLanguageQuery#AttributeRole): Boolean = {
      return getAttributes.contains(v)
    }

    def addAttributes(v: AMLLanguageQuery#AttributeRole): Boolean = {
      if (hasAttributes(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new SystemUnitClassAttributesAttributeRole(this, v)
      return true
    }

    def removeAttributes(v: AMLLanguageQuery#AttributeRole): Boolean = {
      if (!hasAttributes(v)) return false
      attributes.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "SystemUnitClassRole:"
    }

  }

  class AttributeRole extends CAEXObjectRole() {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Attribute(null, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getValueView(): String = {
      +this getValue ()
    }

    def setValueView(value: String, check: CheckingOption.Value): Unit = {
      +this setValue (value)
      connected.addAttributeFilter("value", value.toString(), check)
    }

    override def toString(): String = {
      "AttributeRole:"
    }

  }

  class InstanceHierarchyRole extends CAEXObjectRole() {

    private var internalElements: Set[AMLLanguageQuery#InstanceHierarchyInternalElementsInternalElementRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new InstanceHierarchy(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      internalElements.foreach(_.deleteElement())
    }

    private[AMLLanguageQuery] def removeInternalElementsIntern(v: AMLLanguageQuery#InstanceHierarchyInternalElementsInternalElementRole): Unit = {
      internalElements -= v
    }

    private[AMLLanguageQuery] def setInternalElementsIntern(v: AMLLanguageQuery#InstanceHierarchyInternalElementsInternalElementRole): Unit = {
      internalElements += v
    }

    def getInternalElements(): Set[AMLLanguageQuery#InternalElementRole] = {
      var vs: Set[AMLLanguageQuery#InternalElementRole] = Set.empty
      internalElements.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasInternalElements(v: AMLLanguageQuery#InternalElementRole): Boolean = {
      return getInternalElements.contains(v)
    }

    def addInternalElements(v: AMLLanguageQuery#InternalElementRole): Boolean = {
      if (hasInternalElements(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new InstanceHierarchyInternalElementsInternalElementRole(this, v)
      return true
    }

    def removeInternalElements(v: AMLLanguageQuery#InternalElementRole): Boolean = {
      if (!hasInternalElements(v)) return false
      internalElements.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "InstanceHierarchyRole:"
    }

  }

  class CAEXObjectRole extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new HelperCAEXObject(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    def getIdView(): String = {
      +this getId ()
    }

    def setIdView(id: String, check: CheckingOption.Value): Unit = {
      +this setId (id)
      connected.addAttributeFilter("id", id.toString(), check)
    }

    override def toString(): String = {
      "CAEXObjectRole:"
    }

  }

  class InternalElementRole extends SystemUnitClassRole() {

    private var baseSystemUnit: AMLLanguageQuery#InternalElementBaseSystemUnitSystemUnitClassRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new InternalElement(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (baseSystemUnit != null) baseSystemUnit.deleteElement()
    }

    private[AMLLanguageQuery] def removeBaseSystemUnitIntern(v: AMLLanguageQuery#InternalElementBaseSystemUnitSystemUnitClassRole): Unit = {
      baseSystemUnit = null
    }

    private[AMLLanguageQuery] def setBaseSystemUnitIntern(v: AMLLanguageQuery#InternalElementBaseSystemUnitSystemUnitClassRole): Unit = {
      baseSystemUnit = v
    }

    def getBaseSystemUnit(): AMLLanguageQuery#SystemUnitClassRole = {
      return baseSystemUnit.getTarget()
    }

    def setBaseSystemUnit(v: AMLLanguageQuery#SystemUnitClassRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (baseSystemUnit != null) {
        if (baseSystemUnit.getTarget() == v) return false
        baseSystemUnit.deleteElement()
      }
      new InternalElementBaseSystemUnitSystemUnitClassRole(this, v)
      return true
    }

    override def toString(): String = {
      "InternalElementRole:"
    }

  }

  class SystemUnitClassInternalElementsInternalElementRole(private val source: AMLLanguageQuery#SystemUnitClassRole, private val target: AMLLanguageQuery#InternalElementRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setInternalElementsIntern(this)
      val sp: SystemUnitClass = source.player.right.get.asInstanceOf[SystemUnitClass]
      val tp: InternalElement = target.player.right.get.asInstanceOf[InternalElement]
      val v: SystemUnitClassInternalElementsInternalElement = new SystemUnitClassInternalElementsInternalElement(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeInternalElementsIntern(this)
      super.deleteElement()
    }

    def getSource(): AMLLanguageQuery#SystemUnitClassRole = {
      source
    }

    def getTarget(): AMLLanguageQuery#InternalElementRole = {
      target
    }

    override def toString(): String = {
      "SystemUnitClassInternalElementsInternalElementRole:" + " source=" + source + " target=" + target
    }

  }

  class SystemUnitClassAttributesAttributeRole(private val source: AMLLanguageQuery#SystemUnitClassRole, private val target: AMLLanguageQuery#AttributeRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setAttributesIntern(this)
      val sp: SystemUnitClass = source.player.right.get.asInstanceOf[SystemUnitClass]
      val tp: Attribute = target.player.right.get.asInstanceOf[Attribute]
      val v: SystemUnitClassAttributesAttribute = new SystemUnitClassAttributesAttribute(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeAttributesIntern(this)
      super.deleteElement()
    }

    def getSource(): AMLLanguageQuery#SystemUnitClassRole = {
      source
    }

    def getTarget(): AMLLanguageQuery#AttributeRole = {
      target
    }

    override def toString(): String = {
      "SystemUnitClassAttributesAttributeRole:" + " source=" + source + " target=" + target
    }

  }

  class InstanceHierarchyInternalElementsInternalElementRole(private val source: AMLLanguageQuery#InstanceHierarchyRole, private val target: AMLLanguageQuery#InternalElementRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setInternalElementsIntern(this)
      val sp: InstanceHierarchy = source.player.right.get.asInstanceOf[InstanceHierarchy]
      val tp: InternalElement = target.player.right.get.asInstanceOf[InternalElement]
      val v: InstanceHierarchyInternalElementsInternalElement = new InstanceHierarchyInternalElementsInternalElement(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeInternalElementsIntern(this)
      super.deleteElement()
    }

    def getSource(): AMLLanguageQuery#InstanceHierarchyRole = {
      source
    }

    def getTarget(): AMLLanguageQuery#InternalElementRole = {
      target
    }

    override def toString(): String = {
      "InstanceHierarchyInternalElementsInternalElementRole:" + " source=" + source + " target=" + target
    }

  }

  class InternalElementBaseSystemUnitSystemUnitClassRole(private val source: AMLLanguageQuery#InternalElementRole, private val target: AMLLanguageQuery#SystemUnitClassRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBaseSystemUnitIntern(this)
      val sp: InternalElement = source.player.right.get.asInstanceOf[InternalElement]
      val tp: SystemUnitClass = target.player.right.get.asInstanceOf[SystemUnitClass]
      val v: InternalElementBaseSystemUnitSystemUnitClass = new InternalElementBaseSystemUnitSystemUnitClass(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBaseSystemUnitIntern(this)
      super.deleteElement()
    }

    def getSource(): AMLLanguageQuery#InternalElementRole = {
      source
    }

    def getTarget(): AMLLanguageQuery#SystemUnitClassRole = {
      target
    }

    override def toString(): String = {
      "InternalElementBaseSystemUnitSystemUnitClassRole:" + " source=" + source + " target=" + target
    }

  }

}



    