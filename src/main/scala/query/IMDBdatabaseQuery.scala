package query

import imdbdatabase.User
import imdbdatabase.IMDBFilmsFilm
import imdbdatabase.FilmFiguresFigure
import imdbdatabase.VoteFilmFilm
import imdbdatabase.Film
import imdbdatabase.IMDBVotesVote
import imdbdatabase.VoteUserUser
import imdbdatabase.FigurePlayedByActor
import imdbdatabase.Person
import org.rosi_project.model_management.sum.query.QueryFactory
import imdbdatabase.IMDB
import imdbdatabase.Figure
import imdbdatabase.Actor
import org.rosi_project.model_management.sum.query.CheckingOption
import imdbdatabase.Vote
import imdbdatabase.IMDBUsersUser
import imdbdatabase.IMDBActorsActor

class IMDBdatabaseQuery extends QueryFactory {

  init("IMDBdatabaseQuery")

  def createFigure(): IMDBdatabaseQuery#FigureRole = {
    return new FigureRole()
  }

  def createActor(): IMDBdatabaseQuery#ActorRole = {
    return new ActorRole()
  }

  def createPerson(): IMDBdatabaseQuery#PersonRole = {
    return new PersonRole()
  }

  def createUser(): IMDBdatabaseQuery#UserRole = {
    return new UserRole()
  }

  def createVote(): IMDBdatabaseQuery#VoteRole = {
    return new VoteRole()
  }

  def createIMDB(): IMDBdatabaseQuery#IMDBRole = {
    return new IMDBRole()
  }

  def createFilm(): IMDBdatabaseQuery#FilmRole = {
    return new FilmRole()
  }

  override def toString(): String = {
    "IMDBdatabaseQuery:"
  }

  class IMDBRole extends QueryFactoryElement {

    private var users: Set[IMDBdatabaseQuery#IMDBUsersUserRole] = Set.empty
    private var votes: Set[IMDBdatabaseQuery#IMDBVotesVoteRole] = Set.empty
    private var actors: Set[IMDBdatabaseQuery#IMDBActorsActorRole] = Set.empty
    private var films: Set[IMDBdatabaseQuery#IMDBFilmsFilmRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new IMDB()
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      users.foreach(_.deleteElement())
      votes.foreach(_.deleteElement())
      actors.foreach(_.deleteElement())
      films.foreach(_.deleteElement())
    }

    private[IMDBdatabaseQuery] def removeUsersIntern(v: IMDBdatabaseQuery#IMDBUsersUserRole): Unit = {
      users -= v
    }

    private[IMDBdatabaseQuery] def setUsersIntern(v: IMDBdatabaseQuery#IMDBUsersUserRole): Unit = {
      users += v
    }

    def getUsers(): Set[IMDBdatabaseQuery#UserRole] = {
      var vs: Set[IMDBdatabaseQuery#UserRole] = Set.empty
      users.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasUsers(v: IMDBdatabaseQuery#UserRole): Boolean = {
      return getUsers.contains(v)
    }

    def addUsers(v: IMDBdatabaseQuery#UserRole): Boolean = {
      if (hasUsers(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new IMDBUsersUserRole(this, v)
      return true
    }

    def removeUsers(v: IMDBdatabaseQuery#UserRole): Boolean = {
      if (!hasUsers(v)) return false
      users.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseQuery] def removeVotesIntern(v: IMDBdatabaseQuery#IMDBVotesVoteRole): Unit = {
      votes -= v
    }

    private[IMDBdatabaseQuery] def setVotesIntern(v: IMDBdatabaseQuery#IMDBVotesVoteRole): Unit = {
      votes += v
    }

    def getVotes(): Set[IMDBdatabaseQuery#VoteRole] = {
      var vs: Set[IMDBdatabaseQuery#VoteRole] = Set.empty
      votes.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasVotes(v: IMDBdatabaseQuery#VoteRole): Boolean = {
      return getVotes.contains(v)
    }

    def addVotes(v: IMDBdatabaseQuery#VoteRole): Boolean = {
      if (hasVotes(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new IMDBVotesVoteRole(this, v)
      return true
    }

    def removeVotes(v: IMDBdatabaseQuery#VoteRole): Boolean = {
      if (!hasVotes(v)) return false
      votes.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseQuery] def removeActorsIntern(v: IMDBdatabaseQuery#IMDBActorsActorRole): Unit = {
      actors -= v
    }

    private[IMDBdatabaseQuery] def setActorsIntern(v: IMDBdatabaseQuery#IMDBActorsActorRole): Unit = {
      actors += v
    }

    def getActors(): Set[IMDBdatabaseQuery#ActorRole] = {
      var vs: Set[IMDBdatabaseQuery#ActorRole] = Set.empty
      actors.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasActors(v: IMDBdatabaseQuery#ActorRole): Boolean = {
      return getActors.contains(v)
    }

    def addActors(v: IMDBdatabaseQuery#ActorRole): Boolean = {
      if (hasActors(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new IMDBActorsActorRole(this, v)
      return true
    }

    def removeActors(v: IMDBdatabaseQuery#ActorRole): Boolean = {
      if (!hasActors(v)) return false
      actors.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseQuery] def removeFilmsIntern(v: IMDBdatabaseQuery#IMDBFilmsFilmRole): Unit = {
      films -= v
    }

    private[IMDBdatabaseQuery] def setFilmsIntern(v: IMDBdatabaseQuery#IMDBFilmsFilmRole): Unit = {
      films += v
    }

    def getFilms(): Set[IMDBdatabaseQuery#FilmRole] = {
      var vs: Set[IMDBdatabaseQuery#FilmRole] = Set.empty
      films.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasFilms(v: IMDBdatabaseQuery#FilmRole): Boolean = {
      return getFilms.contains(v)
    }

    def addFilms(v: IMDBdatabaseQuery#FilmRole): Boolean = {
      if (hasFilms(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new IMDBFilmsFilmRole(this, v)
      return true
    }

    def removeFilms(v: IMDBdatabaseQuery#FilmRole): Boolean = {
      if (!hasFilms(v)) return false
      films.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "IMDBRole:"
    }

  }

  class FilmRole extends QueryFactoryElement {

    private var library: IMDBdatabaseQuery#IMDBFilmsFilmRole = null
    private var votes: Set[IMDBdatabaseQuery#VoteFilmFilmRole] = Set.empty
    private var figures: Set[IMDBdatabaseQuery#FilmFiguresFigureRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Film(0, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (library != null) library.deleteElement()
      votes.foreach(_.deleteElement())
      figures.foreach(_.deleteElement())
    }

    def getYearView(): Int = {
      +this getYear ()
    }

    def setYearView(year: Int, check: CheckingOption.Value): Unit = {
      +this setYear (year)
      connected.addAttributeFilter("year", year.toString(), check)
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String, check: CheckingOption.Value): Unit = {
      +this setTitle (title)
      connected.addAttributeFilter("title", title.toString(), check)
    }

    private[IMDBdatabaseQuery] def removeLibraryIntern(v: IMDBdatabaseQuery#IMDBFilmsFilmRole): Unit = {
      library = null
    }

    private[IMDBdatabaseQuery] def setLibraryIntern(v: IMDBdatabaseQuery#IMDBFilmsFilmRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseQuery#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseQuery#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBFilmsFilmRole(v, this)
      return true
    }

    private[IMDBdatabaseQuery] def removeVotesIntern(v: IMDBdatabaseQuery#VoteFilmFilmRole): Unit = {
      votes -= v
    }

    private[IMDBdatabaseQuery] def setVotesIntern(v: IMDBdatabaseQuery#VoteFilmFilmRole): Unit = {
      votes += v
    }

    def getVotes(): Set[IMDBdatabaseQuery#VoteRole] = {
      var vs: Set[IMDBdatabaseQuery#VoteRole] = Set.empty
      votes.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasVotes(v: IMDBdatabaseQuery#VoteRole): Boolean = {
      return getVotes.contains(v)
    }

    def addVotes(v: IMDBdatabaseQuery#VoteRole): Boolean = {
      if (hasVotes(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new VoteFilmFilmRole(v, this)
      return true
    }

    def removeVotes(v: IMDBdatabaseQuery#VoteRole): Boolean = {
      if (!hasVotes(v)) return false
      votes.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseQuery] def removeFiguresIntern(v: IMDBdatabaseQuery#FilmFiguresFigureRole): Unit = {
      figures -= v
    }

    private[IMDBdatabaseQuery] def setFiguresIntern(v: IMDBdatabaseQuery#FilmFiguresFigureRole): Unit = {
      figures += v
    }

    def getFigures(): Set[IMDBdatabaseQuery#FigureRole] = {
      var vs: Set[IMDBdatabaseQuery#FigureRole] = Set.empty
      figures.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasFigures(v: IMDBdatabaseQuery#FigureRole): Boolean = {
      return getFigures.contains(v)
    }

    def addFigures(v: IMDBdatabaseQuery#FigureRole): Boolean = {
      if (hasFigures(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new FilmFiguresFigureRole(this, v)
      return true
    }

    def removeFigures(v: IMDBdatabaseQuery#FigureRole): Boolean = {
      if (!hasFigures(v)) return false
      figures.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "FilmRole:"
    }

  }

  class VoteRole extends QueryFactoryElement {

    private var user: IMDBdatabaseQuery#VoteUserUserRole = null
    private var library: IMDBdatabaseQuery#IMDBVotesVoteRole = null
    private var film: IMDBdatabaseQuery#VoteFilmFilmRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Vote(0)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (user != null) user.deleteElement()
      if (library != null) library.deleteElement()
      if (film != null) film.deleteElement()
    }

    def getScoreView(): Int = {
      +this getScore ()
    }

    def setScoreView(score: Int, check: CheckingOption.Value): Unit = {
      +this setScore (score)
      connected.addAttributeFilter("score", score.toString(), check)
    }

    private[IMDBdatabaseQuery] def removeUserIntern(v: IMDBdatabaseQuery#VoteUserUserRole): Unit = {
      user = null
    }

    private[IMDBdatabaseQuery] def setUserIntern(v: IMDBdatabaseQuery#VoteUserUserRole): Unit = {
      user = v
    }

    def getUser(): IMDBdatabaseQuery#UserRole = {
      return user.getTarget()
    }

    def setUser(v: IMDBdatabaseQuery#UserRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (user != null) {
        if (user.getTarget() == v) return false
        user.deleteElement()
      }
      new VoteUserUserRole(this, v)
      return true
    }

    private[IMDBdatabaseQuery] def removeLibraryIntern(v: IMDBdatabaseQuery#IMDBVotesVoteRole): Unit = {
      library = null
    }

    private[IMDBdatabaseQuery] def setLibraryIntern(v: IMDBdatabaseQuery#IMDBVotesVoteRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseQuery#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseQuery#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBVotesVoteRole(v, this)
      return true
    }

    private[IMDBdatabaseQuery] def removeFilmIntern(v: IMDBdatabaseQuery#VoteFilmFilmRole): Unit = {
      film = null
    }

    private[IMDBdatabaseQuery] def setFilmIntern(v: IMDBdatabaseQuery#VoteFilmFilmRole): Unit = {
      film = v
    }

    def getFilm(): IMDBdatabaseQuery#FilmRole = {
      return film.getTarget()
    }

    def setFilm(v: IMDBdatabaseQuery#FilmRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (film != null) {
        if (film.getTarget() == v) return false
        film.deleteElement()
      }
      new VoteFilmFilmRole(this, v)
      return true
    }

    override def toString(): String = {
      "VoteRole:"
    }

  }

  class ActorRole extends PersonRole() {

    private var plays: Set[IMDBdatabaseQuery#FigurePlayedByActorRole] = Set.empty
    private var library: IMDBdatabaseQuery#IMDBActorsActorRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Actor(0, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      plays.foreach(_.deleteElement())
      if (library != null) library.deleteElement()
    }

    private[IMDBdatabaseQuery] def removePlaysIntern(v: IMDBdatabaseQuery#FigurePlayedByActorRole): Unit = {
      plays -= v
    }

    private[IMDBdatabaseQuery] def setPlaysIntern(v: IMDBdatabaseQuery#FigurePlayedByActorRole): Unit = {
      plays += v
    }

    def getPlays(): Set[IMDBdatabaseQuery#FigureRole] = {
      var vs: Set[IMDBdatabaseQuery#FigureRole] = Set.empty
      plays.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasPlays(v: IMDBdatabaseQuery#FigureRole): Boolean = {
      return getPlays.contains(v)
    }

    def addPlays(v: IMDBdatabaseQuery#FigureRole): Boolean = {
      if (hasPlays(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new FigurePlayedByActorRole(v, this)
      return true
    }

    def removePlays(v: IMDBdatabaseQuery#FigureRole): Boolean = {
      if (!hasPlays(v)) return false
      plays.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseQuery] def removeLibraryIntern(v: IMDBdatabaseQuery#IMDBActorsActorRole): Unit = {
      library = null
    }

    private[IMDBdatabaseQuery] def setLibraryIntern(v: IMDBdatabaseQuery#IMDBActorsActorRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseQuery#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseQuery#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBActorsActorRole(v, this)
      return true
    }

    override def toString(): String = {
      "ActorRole:"
    }

  }

  class PersonRole extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Person(0, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getDobView(): Double = {
      +this getDob ()
    }

    def setDobView(dob: Double, check: CheckingOption.Value): Unit = {
      +this setDob (dob)
      connected.addAttributeFilter("dob", dob.toString(), check)
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    override def toString(): String = {
      "PersonRole:"
    }

  }

  class FigureRole extends QueryFactoryElement {

    private var playedBy: Set[IMDBdatabaseQuery#FigurePlayedByActorRole] = Set.empty
    private var film: IMDBdatabaseQuery#FilmFiguresFigureRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Figure(null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      playedBy.foreach(_.deleteElement())
      if (film != null) film.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    private[IMDBdatabaseQuery] def removePlayedByIntern(v: IMDBdatabaseQuery#FigurePlayedByActorRole): Unit = {
      playedBy -= v
    }

    private[IMDBdatabaseQuery] def setPlayedByIntern(v: IMDBdatabaseQuery#FigurePlayedByActorRole): Unit = {
      playedBy += v
    }

    def getPlayedBy(): Set[IMDBdatabaseQuery#ActorRole] = {
      var vs: Set[IMDBdatabaseQuery#ActorRole] = Set.empty
      playedBy.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasPlayedBy(v: IMDBdatabaseQuery#ActorRole): Boolean = {
      return getPlayedBy.contains(v)
    }

    def addPlayedBy(v: IMDBdatabaseQuery#ActorRole): Boolean = {
      if (hasPlayedBy(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new FigurePlayedByActorRole(this, v)
      return true
    }

    def removePlayedBy(v: IMDBdatabaseQuery#ActorRole): Boolean = {
      if (!hasPlayedBy(v)) return false
      playedBy.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[IMDBdatabaseQuery] def removeFilmIntern(v: IMDBdatabaseQuery#FilmFiguresFigureRole): Unit = {
      film = null
    }

    private[IMDBdatabaseQuery] def setFilmIntern(v: IMDBdatabaseQuery#FilmFiguresFigureRole): Unit = {
      film = v
    }

    def getFilm(): IMDBdatabaseQuery#FilmRole = {
      return film.getSource()
    }

    def setFilm(v: IMDBdatabaseQuery#FilmRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (film != null) {
        if (film.getSource() == v) return false
        film.deleteElement()
      }
      new FilmFiguresFigureRole(v, this)
      return true
    }

    override def toString(): String = {
      "FigureRole:"
    }

  }

  class UserRole extends PersonRole() {

    private var library: IMDBdatabaseQuery#IMDBUsersUserRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new User(null, null, 0, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (library != null) library.deleteElement()
    }

    def getEmailView(): String = {
      +this getEmail ()
    }

    def setEmailView(email: String, check: CheckingOption.Value): Unit = {
      +this setEmail (email)
      connected.addAttributeFilter("email", email.toString(), check)
    }

    def getUserNameView(): String = {
      +this getUserName ()
    }

    def setUserNameView(username: String, check: CheckingOption.Value): Unit = {
      +this setUserName (username)
      connected.addAttributeFilter("userName", username.toString(), check)
    }

    private[IMDBdatabaseQuery] def removeLibraryIntern(v: IMDBdatabaseQuery#IMDBUsersUserRole): Unit = {
      library = null
    }

    private[IMDBdatabaseQuery] def setLibraryIntern(v: IMDBdatabaseQuery#IMDBUsersUserRole): Unit = {
      library = v
    }

    def getLibrary(): IMDBdatabaseQuery#IMDBRole = {
      return library.getSource()
    }

    def setLibrary(v: IMDBdatabaseQuery#IMDBRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (library != null) {
        if (library.getSource() == v) return false
        library.deleteElement()
      }
      new IMDBUsersUserRole(v, this)
      return true
    }

    override def toString(): String = {
      "UserRole:"
    }

  }

  class FigurePlayedByActorRole(private val source: IMDBdatabaseQuery#FigureRole, private val target: IMDBdatabaseQuery#ActorRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setPlaysIntern(this)
      source.setPlayedByIntern(this)
      val sp: Figure = source.player.right.get.asInstanceOf[Figure]
      val tp: Actor = target.player.right.get.asInstanceOf[Actor]
      val v: FigurePlayedByActor = new FigurePlayedByActor(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removePlayedByIntern(this)
      target.removePlaysIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#FigureRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#ActorRole = {
      target
    }

    override def toString(): String = {
      "FigurePlayedByActorRole:" + " source=" + source + " target=" + target
    }

  }

  class VoteUserUserRole(private val source: IMDBdatabaseQuery#VoteRole, private val target: IMDBdatabaseQuery#UserRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setUserIntern(this)
      val sp: Vote = source.player.right.get.asInstanceOf[Vote]
      val tp: User = target.player.right.get.asInstanceOf[User]
      val v: VoteUserUser = new VoteUserUser(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeUserIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#VoteRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#UserRole = {
      target
    }

    override def toString(): String = {
      "VoteUserUserRole:" + " source=" + source + " target=" + target
    }

  }

  class IMDBUsersUserRole(private val source: IMDBdatabaseQuery#IMDBRole, private val target: IMDBdatabaseQuery#UserRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setUsersIntern(this)
      val sp: IMDB = source.player.right.get.asInstanceOf[IMDB]
      val tp: User = target.player.right.get.asInstanceOf[User]
      val v: IMDBUsersUser = new IMDBUsersUser(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeUsersIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#UserRole = {
      target
    }

    override def toString(): String = {
      "IMDBUsersUserRole:" + " source=" + source + " target=" + target
    }

  }

  class IMDBVotesVoteRole(private val source: IMDBdatabaseQuery#IMDBRole, private val target: IMDBdatabaseQuery#VoteRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setVotesIntern(this)
      val sp: IMDB = source.player.right.get.asInstanceOf[IMDB]
      val tp: Vote = target.player.right.get.asInstanceOf[Vote]
      val v: IMDBVotesVote = new IMDBVotesVote(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeVotesIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#VoteRole = {
      target
    }

    override def toString(): String = {
      "IMDBVotesVoteRole:" + " source=" + source + " target=" + target
    }

  }

  class IMDBActorsActorRole(private val source: IMDBdatabaseQuery#IMDBRole, private val target: IMDBdatabaseQuery#ActorRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setActorsIntern(this)
      val sp: IMDB = source.player.right.get.asInstanceOf[IMDB]
      val tp: Actor = target.player.right.get.asInstanceOf[Actor]
      val v: IMDBActorsActor = new IMDBActorsActor(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeActorsIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#ActorRole = {
      target
    }

    override def toString(): String = {
      "IMDBActorsActorRole:" + " source=" + source + " target=" + target
    }

  }

  class IMDBFilmsFilmRole(private val source: IMDBdatabaseQuery#IMDBRole, private val target: IMDBdatabaseQuery#FilmRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setLibraryIntern(this)
      source.setFilmsIntern(this)
      val sp: IMDB = source.player.right.get.asInstanceOf[IMDB]
      val tp: Film = target.player.right.get.asInstanceOf[Film]
      val v: IMDBFilmsFilm = new IMDBFilmsFilm(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeFilmsIntern(this)
      target.removeLibraryIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#IMDBRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#FilmRole = {
      target
    }

    override def toString(): String = {
      "IMDBFilmsFilmRole:" + " source=" + source + " target=" + target
    }

  }

  class VoteFilmFilmRole(private val source: IMDBdatabaseQuery#VoteRole, private val target: IMDBdatabaseQuery#FilmRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setVotesIntern(this)
      source.setFilmIntern(this)
      val sp: Vote = source.player.right.get.asInstanceOf[Vote]
      val tp: Film = target.player.right.get.asInstanceOf[Film]
      val v: VoteFilmFilm = new VoteFilmFilm(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeFilmIntern(this)
      target.removeVotesIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#VoteRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#FilmRole = {
      target
    }

    override def toString(): String = {
      "VoteFilmFilmRole:" + " source=" + source + " target=" + target
    }

  }

  class FilmFiguresFigureRole(private val source: IMDBdatabaseQuery#FilmRole, private val target: IMDBdatabaseQuery#FigureRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setFilmIntern(this)
      source.setFiguresIntern(this)
      val sp: Film = source.player.right.get.asInstanceOf[Film]
      val tp: Figure = target.player.right.get.asInstanceOf[Figure]
      val v: FilmFiguresFigure = new FilmFiguresFigure(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeFiguresIntern(this)
      target.removeFilmIntern(this)
      super.deleteElement()
    }

    def getSource(): IMDBdatabaseQuery#FilmRole = {
      source
    }

    def getTarget(): IMDBdatabaseQuery#FigureRole = {
      target
    }

    override def toString(): String = {
      "FilmFiguresFigureRole:" + " source=" + source + " target=" + target
    }

  }

}



    