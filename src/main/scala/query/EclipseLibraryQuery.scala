package query

import elib.BookAuthorWriter
import elib.BookCategory
import elib.BookOnTapeReaderPerson
import elib.Employee
import elib.LibraryBranchesLibrary
import elib.VideoCassette
import elib.BookOnTapeAuthorWriter
import elib.LibraryBooksBook
import elib.LibraryBorrowersBorrower
import elib.Borrower
import elib.LibraryParentBranchLibrary
import elib.Periodical
import elib.Writer
import elib.AudioVisualItem
import org.rosi_project.model_management.sum.query.QueryFactory
import elib.EmployeeManagerEmployee
import elib.Person
import java.util.Date
import elib.BookOnTape
import elib.VideoCassetteCastPerson
import org.rosi_project.model_management.sum.query.CheckingOption
import elib.Library
import elib.Book
import elib.LibraryWritersWriter
import elib.LibraryEmployeesEmployee
import elib.CirculatingItem
import elib.Item
import elib.LibraryStockItem

class EclipseLibraryQuery extends QueryFactory {

  init("EclipseLibraryQuery")

  def createLibrary(): EclipseLibraryQuery#LibraryRole = {
    return new LibraryRole()
  }

  def createPeriodical(): EclipseLibraryQuery#PeriodicalRole = {
    return new PeriodicalRole()
  }

  def createPerson(): EclipseLibraryQuery#PersonRole = {
    return new PersonRole()
  }

  def createBorrower(): EclipseLibraryQuery#BorrowerRole = {
    return new BorrowerRole()
  }

  def createItem(): EclipseLibraryQuery#ItemRole = {
    return new ItemRole()
  }

  def createAudioVisualItem(): EclipseLibraryQuery#AudioVisualItemRole = {
    return new AudioVisualItemRole()
  }

  def createCirculatingItem(): EclipseLibraryQuery#CirculatingItemRole = {
    return new CirculatingItemRole()
  }

  def createBookOnTape(): EclipseLibraryQuery#BookOnTapeRole = {
    return new BookOnTapeRole()
  }

  def createVideoCassette(): EclipseLibraryQuery#VideoCassetteRole = {
    return new VideoCassetteRole()
  }

  def createBook(): EclipseLibraryQuery#BookRole = {
    return new BookRole()
  }

  def createWriter(): EclipseLibraryQuery#WriterRole = {
    return new WriterRole()
  }

  def createEmployee(): EclipseLibraryQuery#EmployeeRole = {
    return new EmployeeRole()
  }

  override def toString(): String = {
    "EclipseLibraryQuery:"
  }

  class ItemRole extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Item(null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getPublicationDateView(): Date = {
      +this getPublicationDate ()
    }

    def setPublicationDateView(publicationdate: Date, check: CheckingOption.Value): Unit = {
      +this setPublicationDate (publicationdate)
      connected.addAttributeFilter("publicationDate", publicationdate.toString(), check)
    }

    override def toString(): String = {
      "ItemRole:"
    }

  }

  class EmployeeRole extends PersonRole() {

    private var manager: EclipseLibraryQuery#EmployeeManagerEmployeeRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Employee(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (manager != null) manager.deleteElement()
    }

    private[EclipseLibraryQuery] def removeManagerIntern(v: EclipseLibraryQuery#EmployeeManagerEmployeeRole): Unit = {
      manager = null
    }

    private[EclipseLibraryQuery] def setManagerIntern(v: EclipseLibraryQuery#EmployeeManagerEmployeeRole): Unit = {
      manager = v
    }

    def getManager(): EclipseLibraryQuery#EmployeeRole = {
      return manager.getTarget()
    }

    def setManager(v: EclipseLibraryQuery#EmployeeRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (manager != null) {
        if (manager.getTarget() == v) return false
        manager.deleteElement()
      }
      new EmployeeManagerEmployeeRole(this, v)
      return true
    }

    override def toString(): String = {
      "EmployeeRole:"
    }

  }

  class PeriodicalRole extends ItemRole() {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Periodical(0, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getIssuesPerYearView(): Int = {
      +this getIssuesPerYear ()
    }

    def setIssuesPerYearView(issuesperyear: Int, check: CheckingOption.Value): Unit = {
      +this setIssuesPerYear (issuesperyear)
      connected.addAttributeFilter("issuesPerYear", issuesperyear.toString(), check)
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String, check: CheckingOption.Value): Unit = {
      +this setTitle (title)
      connected.addAttributeFilter("title", title.toString(), check)
    }

    override def toString(): String = {
      "PeriodicalRole:"
    }

  }

  class VideoCassetteRole extends AudioVisualItemRole() {

    private var cast: Set[EclipseLibraryQuery#VideoCassetteCastPersonRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new VideoCassette(false, 0, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      cast.foreach(_.deleteElement())
    }

    private[EclipseLibraryQuery] def removeCastIntern(v: EclipseLibraryQuery#VideoCassetteCastPersonRole): Unit = {
      cast -= v
    }

    private[EclipseLibraryQuery] def setCastIntern(v: EclipseLibraryQuery#VideoCassetteCastPersonRole): Unit = {
      cast += v
    }

    def getCast(): Set[EclipseLibraryQuery#PersonRole] = {
      var vs: Set[EclipseLibraryQuery#PersonRole] = Set.empty
      cast.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasCast(v: EclipseLibraryQuery#PersonRole): Boolean = {
      return getCast.contains(v)
    }

    def addCast(v: EclipseLibraryQuery#PersonRole): Boolean = {
      if (hasCast(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new VideoCassetteCastPersonRole(this, v)
      return true
    }

    def removeCast(v: EclipseLibraryQuery#PersonRole): Boolean = {
      if (!hasCast(v)) return false
      cast.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "VideoCassetteRole:"
    }

  }

  class AudioVisualItemRole extends CirculatingItemRole() {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new AudioVisualItem(false, 0, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getDamagedView(): Boolean = {
      +this getDamaged ()
    }

    def setDamagedView(damaged: Boolean, check: CheckingOption.Value): Unit = {
      +this setDamaged (damaged)
      connected.addAttributeFilter("damaged", damaged.toString(), check)
    }

    def getMinutesLengthView(): Int = {
      +this getMinutesLength ()
    }

    def setMinutesLengthView(minuteslength: Int, check: CheckingOption.Value): Unit = {
      +this setMinutesLength (minuteslength)
      connected.addAttributeFilter("minutesLength", minuteslength.toString(), check)
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String, check: CheckingOption.Value): Unit = {
      +this setTitle (title)
      connected.addAttributeFilter("title", title.toString(), check)
    }

    override def toString(): String = {
      "AudioVisualItemRole:"
    }

  }

  class WriterRole extends PersonRole() {

    private var books: Set[EclipseLibraryQuery#BookAuthorWriterRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Writer(null, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      books.foreach(_.deleteElement())
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    private[EclipseLibraryQuery] def removeBooksIntern(v: EclipseLibraryQuery#BookAuthorWriterRole): Unit = {
      books -= v
    }

    private[EclipseLibraryQuery] def setBooksIntern(v: EclipseLibraryQuery#BookAuthorWriterRole): Unit = {
      books += v
    }

    def getBooks(): Set[EclipseLibraryQuery#BookRole] = {
      var vs: Set[EclipseLibraryQuery#BookRole] = Set.empty
      books.foreach { v => vs += v.getSource() }
      return vs
    }

    def hasBooks(v: EclipseLibraryQuery#BookRole): Boolean = {
      return getBooks.contains(v)
    }

    def addBooks(v: EclipseLibraryQuery#BookRole): Boolean = {
      if (hasBooks(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new BookAuthorWriterRole(v, this)
      return true
    }

    def removeBooks(v: EclipseLibraryQuery#BookRole): Boolean = {
      if (!hasBooks(v)) return false
      books.foreach { h =>
        if (h.getSource() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "WriterRole:"
    }

  }

  class BookRole extends CirculatingItemRole() {

    private var author: EclipseLibraryQuery#BookAuthorWriterRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Book(null, 0, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (author != null) author.deleteElement()
    }

    def getCategoryView(): BookCategory.Value = {
      +this getCategory ()
    }

    def setCategoryView(category: BookCategory.Value, check: CheckingOption.Value): Unit = {
      +this setCategory (category)
      connected.addAttributeFilter("category", category.toString(), check)
    }

    def getPagesView(): Int = {
      +this getPages ()
    }

    def setPagesView(pages: Int, check: CheckingOption.Value): Unit = {
      +this setPages (pages)
      connected.addAttributeFilter("pages", pages.toString(), check)
    }

    def getTitleView(): String = {
      +this getTitle ()
    }

    def setTitleView(title: String, check: CheckingOption.Value): Unit = {
      +this setTitle (title)
      connected.addAttributeFilter("title", title.toString(), check)
    }

    private[EclipseLibraryQuery] def removeAuthorIntern(v: EclipseLibraryQuery#BookAuthorWriterRole): Unit = {
      author = null
    }

    private[EclipseLibraryQuery] def setAuthorIntern(v: EclipseLibraryQuery#BookAuthorWriterRole): Unit = {
      author = v
    }

    def getAuthor(): EclipseLibraryQuery#WriterRole = {
      return author.getTarget()
    }

    def setAuthor(v: EclipseLibraryQuery#WriterRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (author != null) {
        if (author.getTarget() == v) return false
        author.deleteElement()
      }
      new BookAuthorWriterRole(this, v)
      return true
    }

    override def toString(): String = {
      "BookRole:"
    }

  }

  class LibraryRole extends QueryFactoryElement {

    private var employees: Set[EclipseLibraryQuery#LibraryEmployeesEmployeeRole] = Set.empty
    private var parentBranch: EclipseLibraryQuery#LibraryParentBranchLibraryRole = null
    private var writers: Set[EclipseLibraryQuery#LibraryWritersWriterRole] = Set.empty
    private var borrowers: Set[EclipseLibraryQuery#LibraryBorrowersBorrowerRole] = Set.empty
    private var stock: Set[EclipseLibraryQuery#LibraryStockItemRole] = Set.empty
    private var branches: Set[EclipseLibraryQuery#LibraryBranchesLibraryRole] = Set.empty
    private var books: Set[EclipseLibraryQuery#LibraryBooksBookRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Library(null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      employees.foreach(_.deleteElement())
      if (parentBranch != null) parentBranch.deleteElement()
      writers.foreach(_.deleteElement())
      borrowers.foreach(_.deleteElement())
      stock.foreach(_.deleteElement())
      branches.foreach(_.deleteElement())
      books.foreach(_.deleteElement())
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    private[EclipseLibraryQuery] def removeEmployeesIntern(v: EclipseLibraryQuery#LibraryEmployeesEmployeeRole): Unit = {
      employees -= v
    }

    private[EclipseLibraryQuery] def setEmployeesIntern(v: EclipseLibraryQuery#LibraryEmployeesEmployeeRole): Unit = {
      employees += v
    }

    def getEmployees(): Set[EclipseLibraryQuery#EmployeeRole] = {
      var vs: Set[EclipseLibraryQuery#EmployeeRole] = Set.empty
      employees.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasEmployees(v: EclipseLibraryQuery#EmployeeRole): Boolean = {
      return getEmployees.contains(v)
    }

    def addEmployees(v: EclipseLibraryQuery#EmployeeRole): Boolean = {
      if (hasEmployees(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryEmployeesEmployeeRole(this, v)
      return true
    }

    def removeEmployees(v: EclipseLibraryQuery#EmployeeRole): Boolean = {
      if (!hasEmployees(v)) return false
      employees.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryQuery] def removeParentBranchIntern(v: EclipseLibraryQuery#LibraryParentBranchLibraryRole): Unit = {
      parentBranch = null
    }

    private[EclipseLibraryQuery] def setParentBranchIntern(v: EclipseLibraryQuery#LibraryParentBranchLibraryRole): Unit = {
      parentBranch = v
    }

    def getParentBranch(): EclipseLibraryQuery#LibraryRole = {
      return parentBranch.getTarget()
    }

    def setParentBranch(v: EclipseLibraryQuery#LibraryRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (parentBranch != null) {
        if (parentBranch.getTarget() == v) return false
        parentBranch.deleteElement()
      }
      new LibraryParentBranchLibraryRole(this, v)
      return true
    }

    private[EclipseLibraryQuery] def removeWritersIntern(v: EclipseLibraryQuery#LibraryWritersWriterRole): Unit = {
      writers -= v
    }

    private[EclipseLibraryQuery] def setWritersIntern(v: EclipseLibraryQuery#LibraryWritersWriterRole): Unit = {
      writers += v
    }

    def getWriters(): Set[EclipseLibraryQuery#WriterRole] = {
      var vs: Set[EclipseLibraryQuery#WriterRole] = Set.empty
      writers.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasWriters(v: EclipseLibraryQuery#WriterRole): Boolean = {
      return getWriters.contains(v)
    }

    def addWriters(v: EclipseLibraryQuery#WriterRole): Boolean = {
      if (hasWriters(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryWritersWriterRole(this, v)
      return true
    }

    def removeWriters(v: EclipseLibraryQuery#WriterRole): Boolean = {
      if (!hasWriters(v)) return false
      writers.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryQuery] def removeBorrowersIntern(v: EclipseLibraryQuery#LibraryBorrowersBorrowerRole): Unit = {
      borrowers -= v
    }

    private[EclipseLibraryQuery] def setBorrowersIntern(v: EclipseLibraryQuery#LibraryBorrowersBorrowerRole): Unit = {
      borrowers += v
    }

    def getBorrowers(): Set[EclipseLibraryQuery#BorrowerRole] = {
      var vs: Set[EclipseLibraryQuery#BorrowerRole] = Set.empty
      borrowers.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasBorrowers(v: EclipseLibraryQuery#BorrowerRole): Boolean = {
      return getBorrowers.contains(v)
    }

    def addBorrowers(v: EclipseLibraryQuery#BorrowerRole): Boolean = {
      if (hasBorrowers(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryBorrowersBorrowerRole(this, v)
      return true
    }

    def removeBorrowers(v: EclipseLibraryQuery#BorrowerRole): Boolean = {
      if (!hasBorrowers(v)) return false
      borrowers.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryQuery] def removeStockIntern(v: EclipseLibraryQuery#LibraryStockItemRole): Unit = {
      stock -= v
    }

    private[EclipseLibraryQuery] def setStockIntern(v: EclipseLibraryQuery#LibraryStockItemRole): Unit = {
      stock += v
    }

    def getStock(): Set[EclipseLibraryQuery#ItemRole] = {
      var vs: Set[EclipseLibraryQuery#ItemRole] = Set.empty
      stock.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasStock(v: EclipseLibraryQuery#ItemRole): Boolean = {
      return getStock.contains(v)
    }

    def addStock(v: EclipseLibraryQuery#ItemRole): Boolean = {
      if (hasStock(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryStockItemRole(this, v)
      return true
    }

    def removeStock(v: EclipseLibraryQuery#ItemRole): Boolean = {
      if (!hasStock(v)) return false
      stock.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryQuery] def removeBranchesIntern(v: EclipseLibraryQuery#LibraryBranchesLibraryRole): Unit = {
      branches -= v
    }

    private[EclipseLibraryQuery] def setBranchesIntern(v: EclipseLibraryQuery#LibraryBranchesLibraryRole): Unit = {
      branches += v
    }

    def getBranches(): Set[EclipseLibraryQuery#LibraryRole] = {
      var vs: Set[EclipseLibraryQuery#LibraryRole] = Set.empty
      branches.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasBranches(v: EclipseLibraryQuery#LibraryRole): Boolean = {
      return getBranches.contains(v)
    }

    def addBranches(v: EclipseLibraryQuery#LibraryRole): Boolean = {
      if (hasBranches(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryBranchesLibraryRole(this, v)
      return true
    }

    def removeBranches(v: EclipseLibraryQuery#LibraryRole): Boolean = {
      if (!hasBranches(v)) return false
      branches.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    private[EclipseLibraryQuery] def removeBooksIntern(v: EclipseLibraryQuery#LibraryBooksBookRole): Unit = {
      books -= v
    }

    private[EclipseLibraryQuery] def setBooksIntern(v: EclipseLibraryQuery#LibraryBooksBookRole): Unit = {
      books += v
    }

    def getBooks(): Set[EclipseLibraryQuery#BookRole] = {
      var vs: Set[EclipseLibraryQuery#BookRole] = Set.empty
      books.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasBooks(v: EclipseLibraryQuery#BookRole): Boolean = {
      return getBooks.contains(v)
    }

    def addBooks(v: EclipseLibraryQuery#BookRole): Boolean = {
      if (hasBooks(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryBooksBookRole(this, v)
      return true
    }

    def removeBooks(v: EclipseLibraryQuery#BookRole): Boolean = {
      if (!hasBooks(v)) return false
      books.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "LibraryRole:"
    }

  }

  class BookOnTapeRole extends AudioVisualItemRole() {

    private var author: EclipseLibraryQuery#BookOnTapeAuthorWriterRole = null
    private var reader: EclipseLibraryQuery#BookOnTapeReaderPersonRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new BookOnTape(false, 0, null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (author != null) author.deleteElement()
      if (reader != null) reader.deleteElement()
    }

    private[EclipseLibraryQuery] def removeAuthorIntern(v: EclipseLibraryQuery#BookOnTapeAuthorWriterRole): Unit = {
      author = null
    }

    private[EclipseLibraryQuery] def setAuthorIntern(v: EclipseLibraryQuery#BookOnTapeAuthorWriterRole): Unit = {
      author = v
    }

    def getAuthor(): EclipseLibraryQuery#WriterRole = {
      return author.getTarget()
    }

    def setAuthor(v: EclipseLibraryQuery#WriterRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (author != null) {
        if (author.getTarget() == v) return false
        author.deleteElement()
      }
      new BookOnTapeAuthorWriterRole(this, v)
      return true
    }

    private[EclipseLibraryQuery] def removeReaderIntern(v: EclipseLibraryQuery#BookOnTapeReaderPersonRole): Unit = {
      reader = null
    }

    private[EclipseLibraryQuery] def setReaderIntern(v: EclipseLibraryQuery#BookOnTapeReaderPersonRole): Unit = {
      reader = v
    }

    def getReader(): EclipseLibraryQuery#PersonRole = {
      return reader.getTarget()
    }

    def setReader(v: EclipseLibraryQuery#PersonRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (reader != null) {
        if (reader.getTarget() == v) return false
        reader.deleteElement()
      }
      new BookOnTapeReaderPersonRole(this, v)
      return true
    }

    override def toString(): String = {
      "BookOnTapeRole:"
    }

  }

  class CirculatingItemRole extends ItemRole() {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new CirculatingItem(null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    override def toString(): String = {
      "CirculatingItemRole:"
    }

  }

  class BorrowerRole extends PersonRole() {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Borrower(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    override def toString(): String = {
      "BorrowerRole:"
    }

  }

  class PersonRole extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Person(null, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getLastNameView(): String = {
      +this getLastName ()
    }

    def setLastNameView(lastname: String, check: CheckingOption.Value): Unit = {
      +this setLastName (lastname)
      connected.addAttributeFilter("lastName", lastname.toString(), check)
    }

    def getFirstNameView(): String = {
      +this getFirstName ()
    }

    def setFirstNameView(firstname: String, check: CheckingOption.Value): Unit = {
      +this setFirstName (firstname)
      connected.addAttributeFilter("firstName", firstname.toString(), check)
    }

    override def toString(): String = {
      "PersonRole:"
    }

  }

  class LibraryEmployeesEmployeeRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#EmployeeRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setEmployeesIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Employee = target.player.right.get.asInstanceOf[Employee]
      val v: LibraryEmployeesEmployee = new LibraryEmployeesEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeEmployeesIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#EmployeeRole = {
      target
    }

    override def toString(): String = {
      "LibraryEmployeesEmployeeRole:" + " source=" + source + " target=" + target
    }

  }

  class LibraryParentBranchLibraryRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#LibraryRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setParentBranchIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Library = target.player.right.get.asInstanceOf[Library]
      val v: LibraryParentBranchLibrary = new LibraryParentBranchLibrary(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeParentBranchIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#LibraryRole = {
      target
    }

    override def toString(): String = {
      "LibraryParentBranchLibraryRole:" + " source=" + source + " target=" + target
    }

  }

  class LibraryWritersWriterRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#WriterRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setWritersIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Writer = target.player.right.get.asInstanceOf[Writer]
      val v: LibraryWritersWriter = new LibraryWritersWriter(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeWritersIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#WriterRole = {
      target
    }

    override def toString(): String = {
      "LibraryWritersWriterRole:" + " source=" + source + " target=" + target
    }

  }

  class BookOnTapeAuthorWriterRole(private val source: EclipseLibraryQuery#BookOnTapeRole, private val target: EclipseLibraryQuery#WriterRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setAuthorIntern(this)
      val sp: BookOnTape = source.player.right.get.asInstanceOf[BookOnTape]
      val tp: Writer = target.player.right.get.asInstanceOf[Writer]
      val v: BookOnTapeAuthorWriter = new BookOnTapeAuthorWriter(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeAuthorIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#BookOnTapeRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#WriterRole = {
      target
    }

    override def toString(): String = {
      "BookOnTapeAuthorWriterRole:" + " source=" + source + " target=" + target
    }

  }

  class BookOnTapeReaderPersonRole(private val source: EclipseLibraryQuery#BookOnTapeRole, private val target: EclipseLibraryQuery#PersonRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setReaderIntern(this)
      val sp: BookOnTape = source.player.right.get.asInstanceOf[BookOnTape]
      val tp: Person = target.player.right.get.asInstanceOf[Person]
      val v: BookOnTapeReaderPerson = new BookOnTapeReaderPerson(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeReaderIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#BookOnTapeRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#PersonRole = {
      target
    }

    override def toString(): String = {
      "BookOnTapeReaderPersonRole:" + " source=" + source + " target=" + target
    }

  }

  class LibraryBorrowersBorrowerRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#BorrowerRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBorrowersIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Borrower = target.player.right.get.asInstanceOf[Borrower]
      val v: LibraryBorrowersBorrower = new LibraryBorrowersBorrower(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBorrowersIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#BorrowerRole = {
      target
    }

    override def toString(): String = {
      "LibraryBorrowersBorrowerRole:" + " source=" + source + " target=" + target
    }

  }

  class LibraryStockItemRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#ItemRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setStockIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Item = target.player.right.get.asInstanceOf[Item]
      val v: LibraryStockItem = new LibraryStockItem(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeStockIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#ItemRole = {
      target
    }

    override def toString(): String = {
      "LibraryStockItemRole:" + " source=" + source + " target=" + target
    }

  }

  class BookAuthorWriterRole(private val source: EclipseLibraryQuery#BookRole, private val target: EclipseLibraryQuery#WriterRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      target.setBooksIntern(this)
      source.setAuthorIntern(this)
      val sp: Book = source.player.right.get.asInstanceOf[Book]
      val tp: Writer = target.player.right.get.asInstanceOf[Writer]
      val v: BookAuthorWriter = new BookAuthorWriter(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeAuthorIntern(this)
      target.removeBooksIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#BookRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#WriterRole = {
      target
    }

    override def toString(): String = {
      "BookAuthorWriterRole:" + " source=" + source + " target=" + target
    }

  }

  class LibraryBranchesLibraryRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#LibraryRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBranchesIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Library = target.player.right.get.asInstanceOf[Library]
      val v: LibraryBranchesLibrary = new LibraryBranchesLibrary(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBranchesIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#LibraryRole = {
      target
    }

    override def toString(): String = {
      "LibraryBranchesLibraryRole:" + " source=" + source + " target=" + target
    }

  }

  class VideoCassetteCastPersonRole(private val source: EclipseLibraryQuery#VideoCassetteRole, private val target: EclipseLibraryQuery#PersonRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setCastIntern(this)
      val sp: VideoCassette = source.player.right.get.asInstanceOf[VideoCassette]
      val tp: Person = target.player.right.get.asInstanceOf[Person]
      val v: VideoCassetteCastPerson = new VideoCassetteCastPerson(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeCastIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#VideoCassetteRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#PersonRole = {
      target
    }

    override def toString(): String = {
      "VideoCassetteCastPersonRole:" + " source=" + source + " target=" + target
    }

  }

  class EmployeeManagerEmployeeRole(private val source: EclipseLibraryQuery#EmployeeRole, private val target: EclipseLibraryQuery#EmployeeRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setManagerIntern(this)
      val sp: Employee = source.player.right.get.asInstanceOf[Employee]
      val tp: Employee = target.player.right.get.asInstanceOf[Employee]
      val v: EmployeeManagerEmployee = new EmployeeManagerEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeManagerIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#EmployeeRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#EmployeeRole = {
      target
    }

    override def toString(): String = {
      "EmployeeManagerEmployeeRole:" + " source=" + source + " target=" + target
    }

  }

  class LibraryBooksBookRole(private val source: EclipseLibraryQuery#LibraryRole, private val target: EclipseLibraryQuery#BookRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setBooksIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Book = target.player.right.get.asInstanceOf[Book]
      val v: LibraryBooksBook = new LibraryBooksBook(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeBooksIntern(this)
      super.deleteElement()
    }

    def getSource(): EclipseLibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): EclipseLibraryQuery#BookRole = {
      target
    }

    override def toString(): String = {
      "LibraryBooksBookRole:" + " source=" + source + " target=" + target
    }

  }

}



    