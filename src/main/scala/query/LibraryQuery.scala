package query

import lib.EmployeeManagerEmployee
import lib.Library
import lib.HelperPerson
import lib.Person
import lib.LibraryEmployeesEmployee
import org.rosi_project.model_management.sum.query.QueryFactory
import lib.Employee
import org.rosi_project.model_management.sum.query.CheckingOption

class LibraryQuery extends QueryFactory {

  init("LibraryQuery")

  def createLibrary(): LibraryQuery#LibraryRole = {
    return new LibraryRole()
  }

  def createEmployee(): LibraryQuery#EmployeeRole = {
    return new EmployeeRole()
  }

  def createPerson(): LibraryQuery#PersonRole = {
    return new PersonRole()
  }

  override def toString(): String = {
    "LibraryQuery:"
  }

  class LibraryRole extends QueryFactoryElement {

    private var employees: Set[LibraryQuery#LibraryEmployeesEmployeeRole] = Set.empty

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Library(null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      employees.foreach(_.deleteElement())
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    private[LibraryQuery] def removeEmployeesIntern(v: LibraryQuery#LibraryEmployeesEmployeeRole): Unit = {
      employees -= v
    }

    private[LibraryQuery] def setEmployeesIntern(v: LibraryQuery#LibraryEmployeesEmployeeRole): Unit = {
      employees += v
    }

    def getEmployees(): Set[LibraryQuery#EmployeeRole] = {
      var vs: Set[LibraryQuery#EmployeeRole] = Set.empty
      employees.foreach { v => vs += v.getTarget() }
      return vs
    }

    def hasEmployees(v: LibraryQuery#EmployeeRole): Boolean = {
      return getEmployees.contains(v)
    }

    def addEmployees(v: LibraryQuery#EmployeeRole): Boolean = {
      if (hasEmployees(v) || !containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      new LibraryEmployeesEmployeeRole(this, v)
      return true
    }

    def removeEmployees(v: LibraryQuery#EmployeeRole): Boolean = {
      if (!hasEmployees(v)) return false
      employees.foreach { h =>
        if (h.getTarget() == v) {
          h.deleteElement()
          return true
        }
      }
      return true
    }

    override def toString(): String = {
      "LibraryRole:"
    }

  }

  class EmployeeRole extends PersonRole() {

    private var manager: LibraryQuery#EmployeeManagerEmployeeRole = null

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new Employee(0, null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
      if (manager != null) manager.deleteElement()
    }

    def getSalaryView(): Double = {
      +this getSalary ()
    }

    def setSalaryView(salary: Double, check: CheckingOption.Value): Unit = {
      +this setSalary (salary)
      connected.addAttributeFilter("salary", salary.toString(), check)
    }

    private[LibraryQuery] def removeManagerIntern(v: LibraryQuery#EmployeeManagerEmployeeRole): Unit = {
      manager = null
    }

    private[LibraryQuery] def setManagerIntern(v: LibraryQuery#EmployeeManagerEmployeeRole): Unit = {
      manager = v
    }

    def getManager(): LibraryQuery#EmployeeRole = {
      return manager.getTarget()
    }

    def setManager(v: LibraryQuery#EmployeeRole): Boolean = {
      if (v == null) return false
      if (!containsRole(v.asInstanceOf[QueryFactoryElement])) return false
      if (manager != null) {
        if (manager.getTarget() == v) return false
        manager.deleteElement()
      }
      new EmployeeManagerEmployeeRole(this, v)
      return true
    }

    override def toString(): String = {
      "EmployeeRole:"
    }

  }

  class PersonRole extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return false
    }

    override protected def getCreationObject(): Object = {
      return new HelperPerson(null)
    }

    override def deleteElement(): Unit = {
      super.deleteElement()
    }

    def getNameView(): String = {
      +this getName ()
    }

    def setNameView(name: String, check: CheckingOption.Value): Unit = {
      +this setName (name)
      connected.addAttributeFilter("name", name.toString(), check)
    }

    override def toString(): String = {
      "PersonRole:"
    }

  }

  class LibraryEmployeesEmployeeRole(private val source: LibraryQuery#LibraryRole, private val target: LibraryQuery#EmployeeRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setEmployeesIntern(this)
      val sp: Library = source.player.right.get.asInstanceOf[Library]
      val tp: Employee = target.player.right.get.asInstanceOf[Employee]
      val v: LibraryEmployeesEmployee = new LibraryEmployeesEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeEmployeesIntern(this)
      super.deleteElement()
    }

    def getSource(): LibraryQuery#LibraryRole = {
      source
    }

    def getTarget(): LibraryQuery#EmployeeRole = {
      target
    }

    override def toString(): String = {
      "LibraryEmployeesEmployeeRole:" + " source=" + source + " target=" + target
    }

  }

  class EmployeeManagerEmployeeRole(private val source: LibraryQuery#EmployeeRole, private val target: LibraryQuery#EmployeeRole) extends QueryFactoryElement {

    override protected def isRelational(): Boolean = {
      return true
    }

    override protected def getCreationObject(): Object = {
      source.setManagerIntern(this)
      val sp: Employee = source.player.right.get.asInstanceOf[Employee]
      val tp: Employee = target.player.right.get.asInstanceOf[Employee]
      val v: EmployeeManagerEmployee = new EmployeeManagerEmployee(sp, tp)
      return v
    }

    override def deleteElement(): Unit = {
      source.removeManagerIntern(this)
      super.deleteElement()
    }

    def getSource(): LibraryQuery#EmployeeRole = {
      source
    }

    def getTarget(): LibraryQuery#EmployeeRole = {
      target
    }

    override def toString(): String = {
      "EmployeeManagerEmployeeRole:" + " source=" + source + " target=" + target
    }

  }

}



    