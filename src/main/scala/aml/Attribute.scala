package aml

class Attribute(protected var value: String, c_Name: String, c_Id: String) extends CAEXObject(c_Name, c_Id) {

  def getValue(): String = {
    value
  }

  def setValue(v: String): Unit = {
    value = v
  }

  override def toString(): String = {
    "Attribute:" + " value=" + value + " name=" + name + " id=" + id
  }

}



    