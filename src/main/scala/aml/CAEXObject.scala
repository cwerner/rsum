package aml

abstract class CAEXObject(protected var name: String, protected var id: String) {

  def getName(): String = {
    name
  }

  def setName(n: String): Unit = {
    name = n
  }

  def getId(): String = {
    id
  }

  def setId(i: String): Unit = {
    id = i
  }

  override def toString(): String = {
    "CAEXObject:" + " name=" + name + " id=" + id
  }

}



    