package aml

import org.rosi_project.model_management.sum.compartments.IDirectComposition

class InstanceHierarchyInternalElementsInternalElement(private val sInstance: InstanceHierarchy, private val tInstance: InternalElement) extends IDirectComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[InstanceHierarchyInternalElementsInternalElement " + source + ", " + target + "]"
  }

  def getSourceIns(): InstanceHierarchy = {
    return sInstance
  }

  def getTargetIns(): InternalElement = {
    return tInstance
  }

  class Source extends IDirectCompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectCompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    