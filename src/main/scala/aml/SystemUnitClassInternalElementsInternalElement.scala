package aml

import org.rosi_project.model_management.sum.compartments.IDirectComposition

class SystemUnitClassInternalElementsInternalElement(private val sInstance: SystemUnitClass, private val tInstance: InternalElement) extends IDirectComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[SystemUnitClassInternalElementsInternalElement " + source + ", " + target + "]"
  }

  def getSourceIns(): SystemUnitClass = {
    return sInstance
  }

  def getTargetIns(): InternalElement = {
    return tInstance
  }

  class Source extends IDirectCompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectCompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    