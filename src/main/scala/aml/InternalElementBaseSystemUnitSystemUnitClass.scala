package aml

import org.rosi_project.model_management.sum.compartments.IDirectAssoziation

class InternalElementBaseSystemUnitSystemUnitClass(private val sInstance: InternalElement, private val tInstance: SystemUnitClass) extends IDirectAssoziation {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[InternalElementBaseSystemUnitSystemUnitClass " + source + ", " + target + "]"
  }

  def getSourceIns(): InternalElement = {
    return sInstance
  }

  def getTargetIns(): SystemUnitClass = {
    return tInstance
  }

  class Source extends IDirectAssoziationSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectAssoziationTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    