package aml

import org.rosi_project.model_management.sum.compartments.IDirectComposition

class SystemUnitClassAttributesAttribute(private val sInstance: SystemUnitClass, private val tInstance: Attribute) extends IDirectComposition {

  override def internalInitialize(): Unit = {
    this.source = new Source()
    this.target = new Target()
    sInstance play this.source
    tInstance play this.target
  }

  override def toString(): String = {
    "[SystemUnitClassAttributesAttribute " + source + ", " + target + "]"
  }

  def getSourceIns(): SystemUnitClass = {
    return sInstance
  }

  def getTargetIns(): Attribute = {
    return tInstance
  }

  class Source extends IDirectCompositionSource {

    override def toString(): String = {
      "S: (" + sInstance + ")"
    }

  }

  class Target extends IDirectCompositionTarget {

    override def toString(): String = {
      "T: (" + tInstance + ")"
    }

  }

}



    