package aml

import org.rosi_project.model_management.sum.query.QueryHelper

class HelperCAEXObject(c_Name: String, c_Id: String) extends CAEXObject(c_Name, c_Id) with QueryHelper {

  override def equals(that: Any): Boolean = {
    that.isInstanceOf[CAEXObject]
  }

  override def toString(): String = {
    "HelperCAEXObject:" + " name=" + name + " id=" + id
  }

}



    