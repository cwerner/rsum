# A Generic Language for Query and Viewtype Generation By-Example

Supplementary material for the VOSE 2019 publication by Christopher Werner, Manuel Wimmer, and Uwe Aßmann.

The repository shows the creation of queries with a by-example mechanism on the AML metamodel excerpt from the paper.

Combination of RSUM and RSYNC including the query by-example approach.

## Installation

Needs the [SCROLL Scala Library](https://github.com/max-leuthaeuser/SCROLL/) as dependency and named in the build.sbt document.

### Prerequisites

* Java SE Development Kit 8 or 9
* SBT (Scala Build Tool)
   * Version 0.13.* only with Java 1.8
   * from Version 1.* with Java 9
   * SBT sets its version in project/build.properties. Remove it if neccessary.

### Get a Copy of this Repository

git `clone https://git-st.inf.tu-dresden.de/cwerner/rsum`

### Setup your favorite IDE

 - **IntelliJ**: use the built-in SBT importer.
 - **Eclipse**: use the sbteclipse SBT plugin.

## Running example

The [Example](src/main/scala/org/rosi_project/example) folder contains the source code of the running example:

* `ExampleAml.scala` as start class that show the creation of eight queries.
* Creating of a result set for each query from the instance model that is created before.
* Creation of ModelJoin representations for each query.