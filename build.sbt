import sbt.Keys.organization

val scrollVersion = "1.8"

lazy val rsum = (project in file(".")).
  settings(
    name := "ModelManagement",
    organization := "de.tu-dresden.inf.st",
    version := "0.1",
    scalaVersion := "2.12.8",
    sbtVersion := "1.2.8",
    libraryDependencies ++= Seq(
        "com.github.max-leuthaeuser" %% "scroll" % scrollVersion)
  )

